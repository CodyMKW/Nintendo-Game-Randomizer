﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MK8DeluxePage = New MetroFramework.Controls.MetroTabPage()
        Me.checkExcludeGold = New MetroFramework.Controls.MetroCheckBox()
        Me.btnMK8Tweet = New MetroFramework.Controls.MetroButton()
        Me.MK8BattleModeName = New MetroFramework.Controls.MetroLabel()
        Me.MK8BattleModeLabel = New MetroFramework.Controls.MetroLabel()
        Me.checkOnlyCharAndParts = New MetroFramework.Controls.MetroCheckBox()
        Me.checkBattleMode = New MetroFramework.Controls.MetroCheckBox()
        Me.checkCustomMiiNames = New MetroFramework.Controls.MetroCheckBox()
        Me.MiiNameLabel = New MetroFramework.Controls.MetroLabel()
        Me.MiiNameTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MK8TrackName = New MetroFramework.Controls.MetroLabel()
        Me.MK8GliderName = New MetroFramework.Controls.MetroLabel()
        Me.MK8TiresName = New MetroFramework.Controls.MetroLabel()
        Me.MK8VehicleName = New MetroFramework.Controls.MetroLabel()
        Me.MK8CharacterName = New MetroFramework.Controls.MetroLabel()
        Me.MK8GliderLabel = New MetroFramework.Controls.MetroLabel()
        Me.MK8TiresLabel = New MetroFramework.Controls.MetroLabel()
        Me.MK8TrackLabel = New MetroFramework.Controls.MetroLabel()
        Me.MK8VehicleLabel = New MetroFramework.Controls.MetroLabel()
        Me.MK8CharacterLabel = New MetroFramework.Controls.MetroLabel()
        Me.btnMK8Generate = New MetroFramework.Controls.MetroButton()
        Me.Splatoon2Page = New MetroFramework.Controls.MetroTabPage()
        Me.btnSplatTweet = New MetroFramework.Controls.MetroButton()
        Me.checkOnlySloshers = New MetroFramework.Controls.MetroCheckBox()
        Me.checkOnlySplatlings = New MetroFramework.Controls.MetroCheckBox()
        Me.checkOnlyBrellas = New MetroFramework.Controls.MetroCheckBox()
        Me.checkOnlyDuelies = New MetroFramework.Controls.MetroCheckBox()
        Me.checkOnlyChargers = New MetroFramework.Controls.MetroCheckBox()
        Me.checkOnlyRollers = New MetroFramework.Controls.MetroCheckBox()
        Me.checkOnlyBlasters = New MetroFramework.Controls.MetroCheckBox()
        Me.checkOnlyShooters = New MetroFramework.Controls.MetroCheckBox()
        Me.checkAllWeapons = New MetroFramework.Controls.MetroCheckBox()
        Me.checkOnlyWeaponClothes = New MetroFramework.Controls.MetroCheckBox()
        Me.btnSplatGenerate = New MetroFramework.Controls.MetroButton()
        Me.SplatModeName = New MetroFramework.Controls.MetroLabel()
        Me.SplatStageName = New MetroFramework.Controls.MetroLabel()
        Me.SplatShoesName = New MetroFramework.Controls.MetroLabel()
        Me.SplatClothingName = New MetroFramework.Controls.MetroLabel()
        Me.SplatHeadgearName = New MetroFramework.Controls.MetroLabel()
        Me.SplatWeaponName = New MetroFramework.Controls.MetroLabel()
        Me.SplatModeLabel = New MetroFramework.Controls.MetroLabel()
        Me.SplatShoesLabel = New MetroFramework.Controls.MetroLabel()
        Me.SplatClothingLabel = New MetroFramework.Controls.MetroLabel()
        Me.SplatStageLabel = New MetroFramework.Controls.MetroLabel()
        Me.SplatWeaponLabel = New MetroFramework.Controls.MetroLabel()
        Me.SplatHeadgearLabel = New MetroFramework.Controls.MetroLabel()
        Me.SmashBrosUltimatePage = New MetroFramework.Controls.MetroTabPage()
        Me.SmashStageName = New MetroFramework.Controls.MetroLabel()
        Me.SmashStageLabel = New MetroFramework.Controls.MetroLabel()
        Me.SmashCharacterName = New MetroFramework.Controls.MetroLabel()
        Me.btnSmashTweet = New MetroFramework.Controls.MetroButton()
        Me.btnSmashGenerate = New MetroFramework.Controls.MetroButton()
        Me.SmashCharacterLabel = New MetroFramework.Controls.MetroLabel()
        Me.AppSettings = New MetroFramework.Controls.MetroTabPage()
        Me.SettingsLocationLabel = New MetroFramework.Controls.MetroLabel()
        Me.warnAboutNewVersion = New MetroFramework.Controls.MetroCheckBox()
        Me.ColorLabel = New MetroFramework.Controls.MetroLabel()
        Me.ThemeLabel = New MetroFramework.Controls.MetroLabel()
        Me.ColorComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.ThemeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.saveSettings = New MetroFramework.Controls.MetroCheckBox()
        Me.btnExitApplication = New MetroFramework.Controls.MetroButton()
        Me.MetroToolTip1 = New MetroFramework.Components.MetroToolTip()
        Me.btnAbout = New MetroFramework.Controls.MetroButton()
        Me.btnDiscord = New MetroFramework.Controls.MetroButton()
        Me.btnTwitter = New MetroFramework.Controls.MetroButton()
        Me.MetroTabControl1.SuspendLayout()
        Me.MK8DeluxePage.SuspendLayout()
        Me.Splatoon2Page.SuspendLayout()
        Me.SmashBrosUltimatePage.SuspendLayout()
        Me.AppSettings.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Controls.Add(Me.MK8DeluxePage)
        Me.MetroTabControl1.Controls.Add(Me.Splatoon2Page)
        Me.MetroTabControl1.Controls.Add(Me.SmashBrosUltimatePage)
        Me.MetroTabControl1.Controls.Add(Me.AppSettings)
        Me.MetroTabControl1.Location = New System.Drawing.Point(23, 63)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 0
        Me.MetroTabControl1.Size = New System.Drawing.Size(520, 198)
        Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Green
        Me.MetroTabControl1.TabIndex = 0
        Me.MetroTabControl1.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8DeluxePage
        '
        Me.MK8DeluxePage.Controls.Add(Me.checkExcludeGold)
        Me.MK8DeluxePage.Controls.Add(Me.btnMK8Tweet)
        Me.MK8DeluxePage.Controls.Add(Me.MK8BattleModeName)
        Me.MK8DeluxePage.Controls.Add(Me.MK8BattleModeLabel)
        Me.MK8DeluxePage.Controls.Add(Me.checkOnlyCharAndParts)
        Me.MK8DeluxePage.Controls.Add(Me.checkBattleMode)
        Me.MK8DeluxePage.Controls.Add(Me.checkCustomMiiNames)
        Me.MK8DeluxePage.Controls.Add(Me.MiiNameLabel)
        Me.MK8DeluxePage.Controls.Add(Me.MiiNameTextBox)
        Me.MK8DeluxePage.Controls.Add(Me.MetroLabel1)
        Me.MK8DeluxePage.Controls.Add(Me.MK8TrackName)
        Me.MK8DeluxePage.Controls.Add(Me.MK8GliderName)
        Me.MK8DeluxePage.Controls.Add(Me.MK8TiresName)
        Me.MK8DeluxePage.Controls.Add(Me.MK8VehicleName)
        Me.MK8DeluxePage.Controls.Add(Me.MK8CharacterName)
        Me.MK8DeluxePage.Controls.Add(Me.MK8GliderLabel)
        Me.MK8DeluxePage.Controls.Add(Me.MK8TiresLabel)
        Me.MK8DeluxePage.Controls.Add(Me.MK8TrackLabel)
        Me.MK8DeluxePage.Controls.Add(Me.MK8VehicleLabel)
        Me.MK8DeluxePage.Controls.Add(Me.MK8CharacterLabel)
        Me.MK8DeluxePage.Controls.Add(Me.btnMK8Generate)
        Me.MK8DeluxePage.HorizontalScrollbarBarColor = True
        Me.MK8DeluxePage.Location = New System.Drawing.Point(4, 35)
        Me.MK8DeluxePage.Name = "MK8DeluxePage"
        Me.MK8DeluxePage.Size = New System.Drawing.Size(512, 159)
        Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8DeluxePage.TabIndex = 0
        Me.MK8DeluxePage.Text = "Mario Kart 8 Deluxe"
        Me.MK8DeluxePage.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MK8DeluxePage.VerticalScrollbarBarColor = True
        '
        'checkExcludeGold
        '
        Me.checkExcludeGold.AutoSize = True
        Me.checkExcludeGold.Location = New System.Drawing.Point(309, 68)
        Me.checkExcludeGold.Name = "checkExcludeGold"
        Me.checkExcludeGold.Size = New System.Drawing.Size(205, 15)
        Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Green
        Me.checkExcludeGold.TabIndex = 27
        Me.checkExcludeGold.Text = "Exclude Gold Mario and Gold Parts"
        Me.checkExcludeGold.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.checkExcludeGold.UseVisualStyleBackColor = True
        '
        'btnMK8Tweet
        '
        Me.btnMK8Tweet.Enabled = False
        Me.btnMK8Tweet.Location = New System.Drawing.Point(259, 133)
        Me.btnMK8Tweet.Name = "btnMK8Tweet"
        Me.btnMK8Tweet.Size = New System.Drawing.Size(250, 23)
        Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Green
        Me.btnMK8Tweet.TabIndex = 26
        Me.btnMK8Tweet.Text = "Tweet Results"
        Me.btnMK8Tweet.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8BattleModeName
        '
        Me.MK8BattleModeName.AutoSize = True
        Me.MK8BattleModeName.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MK8BattleModeName.Location = New System.Drawing.Point(61, 102)
        Me.MK8BattleModeName.Name = "MK8BattleModeName"
        Me.MK8BattleModeName.Size = New System.Drawing.Size(0, 0)
        Me.MK8BattleModeName.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8BattleModeName.TabIndex = 25
        Me.MK8BattleModeName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8BattleModeLabel
        '
        Me.MK8BattleModeLabel.AutoSize = True
        Me.MK8BattleModeLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MK8BattleModeLabel.Location = New System.Drawing.Point(3, 102)
        Me.MK8BattleModeLabel.Name = "MK8BattleModeLabel"
        Me.MK8BattleModeLabel.Size = New System.Drawing.Size(0, 0)
        Me.MK8BattleModeLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8BattleModeLabel.TabIndex = 24
        Me.MK8BattleModeLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'checkOnlyCharAndParts
        '
        Me.checkOnlyCharAndParts.AutoSize = True
        Me.checkOnlyCharAndParts.Location = New System.Drawing.Point(309, 49)
        Me.checkOnlyCharAndParts.Name = "checkOnlyCharAndParts"
        Me.checkOnlyCharAndParts.Size = New System.Drawing.Size(200, 15)
        Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlyCharAndParts.TabIndex = 23
        Me.checkOnlyCharAndParts.Text = "Only Characters and Vehicle Parts"
        Me.checkOnlyCharAndParts.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.checkOnlyCharAndParts.UseVisualStyleBackColor = True
        '
        'checkBattleMode
        '
        Me.checkBattleMode.AutoSize = True
        Me.checkBattleMode.Location = New System.Drawing.Point(309, 30)
        Me.checkBattleMode.Name = "checkBattleMode"
        Me.checkBattleMode.Size = New System.Drawing.Size(125, 15)
        Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Green
        Me.checkBattleMode.TabIndex = 22
        Me.checkBattleMode.Text = "Enable Battle Mode"
        Me.checkBattleMode.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.checkBattleMode.UseVisualStyleBackColor = True
        '
        'checkCustomMiiNames
        '
        Me.checkCustomMiiNames.AutoSize = True
        Me.checkCustomMiiNames.Location = New System.Drawing.Point(309, 11)
        Me.checkCustomMiiNames.Name = "checkCustomMiiNames"
        Me.checkCustomMiiNames.Size = New System.Drawing.Size(118, 15)
        Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Green
        Me.checkCustomMiiNames.TabIndex = 21
        Me.checkCustomMiiNames.Text = "Enable Mii Names"
        Me.checkCustomMiiNames.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.checkCustomMiiNames.UseVisualStyleBackColor = True
        '
        'MiiNameLabel
        '
        Me.MiiNameLabel.AutoSize = True
        Me.MiiNameLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MiiNameLabel.Location = New System.Drawing.Point(305, 0)
        Me.MiiNameLabel.Name = "MiiNameLabel"
        Me.MiiNameLabel.Size = New System.Drawing.Size(78, 19)
        Me.MiiNameLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.MiiNameLabel.TabIndex = 20
        Me.MiiNameLabel.Text = "Mii Name:"
        Me.MiiNameLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MiiNameLabel.Visible = False
        '
        'MiiNameTextBox
        '
        Me.MiiNameTextBox.Enabled = False
        Me.MiiNameTextBox.Location = New System.Drawing.Point(309, 22)
        Me.MiiNameTextBox.MaxLength = 10
        Me.MiiNameTextBox.Name = "MiiNameTextBox"
        Me.MiiNameTextBox.Size = New System.Drawing.Size(118, 23)
        Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Green
        Me.MiiNameTextBox.TabIndex = 19
        Me.MiiNameTextBox.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MiiNameTextBox.Visible = False
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel1.Location = New System.Drawing.Point(54, 45)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(0, 0)
        Me.MetroLabel1.Style = MetroFramework.MetroColorStyle.Green
        Me.MetroLabel1.TabIndex = 18
        Me.MetroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8TrackName
        '
        Me.MK8TrackName.AutoSize = True
        Me.MK8TrackName.Location = New System.Drawing.Point(59, 83)
        Me.MK8TrackName.Name = "MK8TrackName"
        Me.MK8TrackName.Size = New System.Drawing.Size(0, 0)
        Me.MK8TrackName.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8TrackName.TabIndex = 12
        Me.MK8TrackName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8GliderName
        '
        Me.MK8GliderName.AutoSize = True
        Me.MK8GliderName.Location = New System.Drawing.Point(63, 64)
        Me.MK8GliderName.Name = "MK8GliderName"
        Me.MK8GliderName.Size = New System.Drawing.Size(0, 0)
        Me.MK8GliderName.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8GliderName.TabIndex = 11
        Me.MK8GliderName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8TiresName
        '
        Me.MK8TiresName.AutoSize = True
        Me.MK8TiresName.Location = New System.Drawing.Point(54, 45)
        Me.MK8TiresName.Name = "MK8TiresName"
        Me.MK8TiresName.Size = New System.Drawing.Size(0, 0)
        Me.MK8TiresName.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8TiresName.TabIndex = 10
        Me.MK8TiresName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8VehicleName
        '
        Me.MK8VehicleName.AutoSize = True
        Me.MK8VehicleName.Location = New System.Drawing.Point(70, 26)
        Me.MK8VehicleName.Name = "MK8VehicleName"
        Me.MK8VehicleName.Size = New System.Drawing.Size(0, 0)
        Me.MK8VehicleName.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8VehicleName.TabIndex = 9
        Me.MK8VehicleName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8CharacterName
        '
        Me.MK8CharacterName.AutoSize = True
        Me.MK8CharacterName.Location = New System.Drawing.Point(87, 7)
        Me.MK8CharacterName.Name = "MK8CharacterName"
        Me.MK8CharacterName.Size = New System.Drawing.Size(0, 0)
        Me.MK8CharacterName.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8CharacterName.TabIndex = 8
        Me.MK8CharacterName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8GliderLabel
        '
        Me.MK8GliderLabel.AutoSize = True
        Me.MK8GliderLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MK8GliderLabel.Location = New System.Drawing.Point(3, 64)
        Me.MK8GliderLabel.Name = "MK8GliderLabel"
        Me.MK8GliderLabel.Size = New System.Drawing.Size(54, 19)
        Me.MK8GliderLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8GliderLabel.TabIndex = 7
        Me.MK8GliderLabel.Text = "Glider:"
        Me.MK8GliderLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8TiresLabel
        '
        Me.MK8TiresLabel.AutoSize = True
        Me.MK8TiresLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MK8TiresLabel.Location = New System.Drawing.Point(3, 45)
        Me.MK8TiresLabel.Name = "MK8TiresLabel"
        Me.MK8TiresLabel.Size = New System.Drawing.Size(45, 19)
        Me.MK8TiresLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8TiresLabel.TabIndex = 6
        Me.MK8TiresLabel.Text = "Tires:"
        Me.MK8TiresLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8TrackLabel
        '
        Me.MK8TrackLabel.AutoSize = True
        Me.MK8TrackLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MK8TrackLabel.Location = New System.Drawing.Point(3, 83)
        Me.MK8TrackLabel.Name = "MK8TrackLabel"
        Me.MK8TrackLabel.Size = New System.Drawing.Size(50, 19)
        Me.MK8TrackLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8TrackLabel.TabIndex = 5
        Me.MK8TrackLabel.Text = "Track:"
        Me.MK8TrackLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8VehicleLabel
        '
        Me.MK8VehicleLabel.AutoSize = True
        Me.MK8VehicleLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MK8VehicleLabel.Location = New System.Drawing.Point(3, 26)
        Me.MK8VehicleLabel.Name = "MK8VehicleLabel"
        Me.MK8VehicleLabel.Size = New System.Drawing.Size(61, 19)
        Me.MK8VehicleLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8VehicleLabel.TabIndex = 4
        Me.MK8VehicleLabel.Text = "Vehicle:"
        Me.MK8VehicleLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MK8CharacterLabel
        '
        Me.MK8CharacterLabel.AutoSize = True
        Me.MK8CharacterLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MK8CharacterLabel.Location = New System.Drawing.Point(3, 7)
        Me.MK8CharacterLabel.Name = "MK8CharacterLabel"
        Me.MK8CharacterLabel.Size = New System.Drawing.Size(78, 19)
        Me.MK8CharacterLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.MK8CharacterLabel.TabIndex = 3
        Me.MK8CharacterLabel.Text = "Character:"
        Me.MK8CharacterLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'btnMK8Generate
        '
        Me.btnMK8Generate.Location = New System.Drawing.Point(3, 133)
        Me.btnMK8Generate.Name = "btnMK8Generate"
        Me.btnMK8Generate.Size = New System.Drawing.Size(250, 23)
        Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Green
        Me.btnMK8Generate.TabIndex = 2
        Me.btnMK8Generate.Text = "Generate"
        Me.btnMK8Generate.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'Splatoon2Page
        '
        Me.Splatoon2Page.Controls.Add(Me.btnSplatTweet)
        Me.Splatoon2Page.Controls.Add(Me.checkOnlySloshers)
        Me.Splatoon2Page.Controls.Add(Me.checkOnlySplatlings)
        Me.Splatoon2Page.Controls.Add(Me.checkOnlyBrellas)
        Me.Splatoon2Page.Controls.Add(Me.checkOnlyDuelies)
        Me.Splatoon2Page.Controls.Add(Me.checkOnlyChargers)
        Me.Splatoon2Page.Controls.Add(Me.checkOnlyRollers)
        Me.Splatoon2Page.Controls.Add(Me.checkOnlyBlasters)
        Me.Splatoon2Page.Controls.Add(Me.checkOnlyShooters)
        Me.Splatoon2Page.Controls.Add(Me.checkAllWeapons)
        Me.Splatoon2Page.Controls.Add(Me.checkOnlyWeaponClothes)
        Me.Splatoon2Page.Controls.Add(Me.btnSplatGenerate)
        Me.Splatoon2Page.Controls.Add(Me.SplatModeName)
        Me.Splatoon2Page.Controls.Add(Me.SplatStageName)
        Me.Splatoon2Page.Controls.Add(Me.SplatShoesName)
        Me.Splatoon2Page.Controls.Add(Me.SplatClothingName)
        Me.Splatoon2Page.Controls.Add(Me.SplatHeadgearName)
        Me.Splatoon2Page.Controls.Add(Me.SplatWeaponName)
        Me.Splatoon2Page.Controls.Add(Me.SplatModeLabel)
        Me.Splatoon2Page.Controls.Add(Me.SplatShoesLabel)
        Me.Splatoon2Page.Controls.Add(Me.SplatClothingLabel)
        Me.Splatoon2Page.Controls.Add(Me.SplatStageLabel)
        Me.Splatoon2Page.Controls.Add(Me.SplatWeaponLabel)
        Me.Splatoon2Page.Controls.Add(Me.SplatHeadgearLabel)
        Me.Splatoon2Page.HorizontalScrollbarBarColor = True
        Me.Splatoon2Page.Location = New System.Drawing.Point(4, 35)
        Me.Splatoon2Page.Name = "Splatoon2Page"
        Me.Splatoon2Page.Size = New System.Drawing.Size(512, 159)
        Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Green
        Me.Splatoon2Page.TabIndex = 1
        Me.Splatoon2Page.Text = "Splatoon 2"
        Me.Splatoon2Page.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.Splatoon2Page.VerticalScrollbarBarColor = True
        '
        'btnSplatTweet
        '
        Me.btnSplatTweet.Enabled = False
        Me.btnSplatTweet.Location = New System.Drawing.Point(259, 133)
        Me.btnSplatTweet.Name = "btnSplatTweet"
        Me.btnSplatTweet.Size = New System.Drawing.Size(250, 23)
        Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Green
        Me.btnSplatTweet.TabIndex = 27
        Me.btnSplatTweet.Text = "Tweet Results"
        Me.btnSplatTweet.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'checkOnlySloshers
        '
        Me.checkOnlySloshers.AutoSize = True
        Me.checkOnlySloshers.Location = New System.Drawing.Point(308, 110)
        Me.checkOnlySloshers.Name = "checkOnlySloshers"
        Me.checkOnlySloshers.Size = New System.Drawing.Size(94, 15)
        Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlySloshers.TabIndex = 33
        Me.checkOnlySloshers.Text = "Only Sloshers"
        Me.checkOnlySloshers.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.checkOnlySloshers, "Contains 9 sloshers")
        Me.checkOnlySloshers.UseVisualStyleBackColor = True
        '
        'checkOnlySplatlings
        '
        Me.checkOnlySplatlings.AutoSize = True
        Me.checkOnlySplatlings.Location = New System.Drawing.Point(407, 49)
        Me.checkOnlySplatlings.Name = "checkOnlySplatlings"
        Me.checkOnlySplatlings.Size = New System.Drawing.Size(102, 15)
        Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlySplatlings.TabIndex = 32
        Me.checkOnlySplatlings.Text = "Only Splatlings"
        Me.checkOnlySplatlings.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.checkOnlySplatlings, "Contains 9 splatlings")
        Me.checkOnlySplatlings.UseVisualStyleBackColor = True
        '
        'checkOnlyBrellas
        '
        Me.checkOnlyBrellas.AutoSize = True
        Me.checkOnlyBrellas.Location = New System.Drawing.Point(407, 89)
        Me.checkOnlyBrellas.Name = "checkOnlyBrellas"
        Me.checkOnlyBrellas.Size = New System.Drawing.Size(85, 15)
        Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlyBrellas.TabIndex = 31
        Me.checkOnlyBrellas.Text = "Only Brellas"
        Me.checkOnlyBrellas.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.checkOnlyBrellas, "Contains 7 brellas")
        Me.checkOnlyBrellas.UseVisualStyleBackColor = True
        '
        'checkOnlyDuelies
        '
        Me.checkOnlyDuelies.AutoSize = True
        Me.checkOnlyDuelies.Location = New System.Drawing.Point(407, 68)
        Me.checkOnlyDuelies.Name = "checkOnlyDuelies"
        Me.checkOnlyDuelies.Size = New System.Drawing.Size(89, 15)
        Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlyDuelies.TabIndex = 30
        Me.checkOnlyDuelies.Text = "Only Duelies"
        Me.checkOnlyDuelies.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.checkOnlyDuelies, "Contains 12 duelies")
        Me.checkOnlyDuelies.UseVisualStyleBackColor = True
        '
        'checkOnlyChargers
        '
        Me.checkOnlyChargers.AutoSize = True
        Me.checkOnlyChargers.Location = New System.Drawing.Point(407, 30)
        Me.checkOnlyChargers.Name = "checkOnlyChargers"
        Me.checkOnlyChargers.Size = New System.Drawing.Size(98, 15)
        Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlyChargers.TabIndex = 29
        Me.checkOnlyChargers.Text = "Only Chargers"
        Me.checkOnlyChargers.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.checkOnlyChargers, "Contains 17 chargers")
        Me.checkOnlyChargers.UseVisualStyleBackColor = True
        '
        'checkOnlyRollers
        '
        Me.checkOnlyRollers.AutoSize = True
        Me.checkOnlyRollers.Location = New System.Drawing.Point(308, 89)
        Me.checkOnlyRollers.Name = "checkOnlyRollers"
        Me.checkOnlyRollers.Size = New System.Drawing.Size(86, 15)
        Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlyRollers.TabIndex = 28
        Me.checkOnlyRollers.Text = "Only Rollers"
        Me.checkOnlyRollers.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.checkOnlyRollers, "Contains 15 rollers")
        Me.checkOnlyRollers.UseVisualStyleBackColor = True
        '
        'checkOnlyBlasters
        '
        Me.checkOnlyBlasters.AutoSize = True
        Me.checkOnlyBlasters.Location = New System.Drawing.Point(308, 68)
        Me.checkOnlyBlasters.Name = "checkOnlyBlasters"
        Me.checkOnlyBlasters.Size = New System.Drawing.Size(91, 15)
        Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlyBlasters.TabIndex = 27
        Me.checkOnlyBlasters.Text = "Only Blasters"
        Me.checkOnlyBlasters.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.checkOnlyBlasters, "Contains 13 blasters")
        Me.checkOnlyBlasters.UseVisualStyleBackColor = True
        '
        'checkOnlyShooters
        '
        Me.checkOnlyShooters.AutoSize = True
        Me.checkOnlyShooters.Location = New System.Drawing.Point(308, 49)
        Me.checkOnlyShooters.Name = "checkOnlyShooters"
        Me.checkOnlyShooters.Size = New System.Drawing.Size(97, 15)
        Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlyShooters.TabIndex = 26
        Me.checkOnlyShooters.Text = "Only Shooters"
        Me.checkOnlyShooters.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.checkOnlyShooters, "Contains 29 shooters")
        Me.checkOnlyShooters.UseVisualStyleBackColor = True
        '
        'checkAllWeapons
        '
        Me.checkAllWeapons.AutoSize = True
        Me.checkAllWeapons.Checked = True
        Me.checkAllWeapons.CheckState = System.Windows.Forms.CheckState.Checked
        Me.checkAllWeapons.Location = New System.Drawing.Point(308, 30)
        Me.checkAllWeapons.Name = "checkAllWeapons"
        Me.checkAllWeapons.Size = New System.Drawing.Size(89, 15)
        Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Green
        Me.checkAllWeapons.TabIndex = 25
        Me.checkAllWeapons.Text = "All Weapons"
        Me.checkAllWeapons.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.checkAllWeapons, "Contains 111 weapons")
        Me.checkAllWeapons.UseVisualStyleBackColor = True
        '
        'checkOnlyWeaponClothes
        '
        Me.checkOnlyWeaponClothes.AutoSize = True
        Me.checkOnlyWeaponClothes.Location = New System.Drawing.Point(308, 11)
        Me.checkOnlyWeaponClothes.Name = "checkOnlyWeaponClothes"
        Me.checkOnlyWeaponClothes.Size = New System.Drawing.Size(166, 15)
        Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Green
        Me.checkOnlyWeaponClothes.TabIndex = 24
        Me.checkOnlyWeaponClothes.Text = "Only Weapons and Clothes"
        Me.checkOnlyWeaponClothes.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.checkOnlyWeaponClothes.UseVisualStyleBackColor = True
        '
        'btnSplatGenerate
        '
        Me.btnSplatGenerate.Location = New System.Drawing.Point(3, 133)
        Me.btnSplatGenerate.Name = "btnSplatGenerate"
        Me.btnSplatGenerate.Size = New System.Drawing.Size(250, 23)
        Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Green
        Me.btnSplatGenerate.TabIndex = 8
        Me.btnSplatGenerate.Text = "Generate"
        Me.btnSplatGenerate.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatModeName
        '
        Me.SplatModeName.AutoSize = True
        Me.SplatModeName.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.SplatModeName.Location = New System.Drawing.Point(61, 102)
        Me.SplatModeName.Name = "SplatModeName"
        Me.SplatModeName.Size = New System.Drawing.Size(0, 0)
        Me.SplatModeName.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatModeName.TabIndex = 19
        Me.SplatModeName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatStageName
        '
        Me.SplatStageName.AutoSize = True
        Me.SplatStageName.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.SplatStageName.Location = New System.Drawing.Point(60, 83)
        Me.SplatStageName.Name = "SplatStageName"
        Me.SplatStageName.Size = New System.Drawing.Size(0, 0)
        Me.SplatStageName.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatStageName.TabIndex = 18
        Me.SplatStageName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatShoesName
        '
        Me.SplatShoesName.AutoSize = True
        Me.SplatShoesName.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.SplatShoesName.Location = New System.Drawing.Point(61, 64)
        Me.SplatShoesName.Name = "SplatShoesName"
        Me.SplatShoesName.Size = New System.Drawing.Size(0, 0)
        Me.SplatShoesName.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatShoesName.TabIndex = 17
        Me.SplatShoesName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatClothingName
        '
        Me.SplatClothingName.AutoSize = True
        Me.SplatClothingName.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.SplatClothingName.Location = New System.Drawing.Point(53, 45)
        Me.SplatClothingName.Name = "SplatClothingName"
        Me.SplatClothingName.Size = New System.Drawing.Size(0, 0)
        Me.SplatClothingName.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatClothingName.TabIndex = 16
        Me.SplatClothingName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatHeadgearName
        '
        Me.SplatHeadgearName.AutoSize = True
        Me.SplatHeadgearName.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.SplatHeadgearName.Location = New System.Drawing.Point(89, 26)
        Me.SplatHeadgearName.Name = "SplatHeadgearName"
        Me.SplatHeadgearName.Size = New System.Drawing.Size(0, 0)
        Me.SplatHeadgearName.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatHeadgearName.TabIndex = 15
        Me.SplatHeadgearName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatWeaponName
        '
        Me.SplatWeaponName.AutoSize = True
        Me.SplatWeaponName.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.SplatWeaponName.Location = New System.Drawing.Point(78, 7)
        Me.SplatWeaponName.Name = "SplatWeaponName"
        Me.SplatWeaponName.Size = New System.Drawing.Size(0, 0)
        Me.SplatWeaponName.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatWeaponName.TabIndex = 14
        Me.SplatWeaponName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatModeLabel
        '
        Me.SplatModeLabel.AutoSize = True
        Me.SplatModeLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.SplatModeLabel.Location = New System.Drawing.Point(3, 102)
        Me.SplatModeLabel.Name = "SplatModeLabel"
        Me.SplatModeLabel.Size = New System.Drawing.Size(52, 19)
        Me.SplatModeLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatModeLabel.TabIndex = 13
        Me.SplatModeLabel.Text = "Mode:"
        Me.SplatModeLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatShoesLabel
        '
        Me.SplatShoesLabel.AutoSize = True
        Me.SplatShoesLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.SplatShoesLabel.Location = New System.Drawing.Point(3, 64)
        Me.SplatShoesLabel.Name = "SplatShoesLabel"
        Me.SplatShoesLabel.Size = New System.Drawing.Size(52, 19)
        Me.SplatShoesLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatShoesLabel.TabIndex = 12
        Me.SplatShoesLabel.Text = "Shoes:"
        Me.SplatShoesLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatClothingLabel
        '
        Me.SplatClothingLabel.AutoSize = True
        Me.SplatClothingLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.SplatClothingLabel.Location = New System.Drawing.Point(3, 45)
        Me.SplatClothingLabel.Name = "SplatClothingLabel"
        Me.SplatClothingLabel.Size = New System.Drawing.Size(44, 19)
        Me.SplatClothingLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatClothingLabel.TabIndex = 11
        Me.SplatClothingLabel.Text = "Shirt:"
        Me.SplatClothingLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatStageLabel
        '
        Me.SplatStageLabel.AutoSize = True
        Me.SplatStageLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.SplatStageLabel.Location = New System.Drawing.Point(3, 83)
        Me.SplatStageLabel.Name = "SplatStageLabel"
        Me.SplatStageLabel.Size = New System.Drawing.Size(51, 19)
        Me.SplatStageLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatStageLabel.TabIndex = 10
        Me.SplatStageLabel.Text = "Stage:"
        Me.SplatStageLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatWeaponLabel
        '
        Me.SplatWeaponLabel.AutoSize = True
        Me.SplatWeaponLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.SplatWeaponLabel.Location = New System.Drawing.Point(3, 7)
        Me.SplatWeaponLabel.Name = "SplatWeaponLabel"
        Me.SplatWeaponLabel.Size = New System.Drawing.Size(69, 19)
        Me.SplatWeaponLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatWeaponLabel.TabIndex = 8
        Me.SplatWeaponLabel.Text = "Weapon:"
        Me.SplatWeaponLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SplatHeadgearLabel
        '
        Me.SplatHeadgearLabel.AutoSize = True
        Me.SplatHeadgearLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.SplatHeadgearLabel.Location = New System.Drawing.Point(3, 26)
        Me.SplatHeadgearLabel.Name = "SplatHeadgearLabel"
        Me.SplatHeadgearLabel.Size = New System.Drawing.Size(80, 19)
        Me.SplatHeadgearLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.SplatHeadgearLabel.TabIndex = 9
        Me.SplatHeadgearLabel.Text = "Headgear:"
        Me.SplatHeadgearLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SmashBrosUltimatePage
        '
        Me.SmashBrosUltimatePage.Controls.Add(Me.SmashStageName)
        Me.SmashBrosUltimatePage.Controls.Add(Me.SmashStageLabel)
        Me.SmashBrosUltimatePage.Controls.Add(Me.SmashCharacterName)
        Me.SmashBrosUltimatePage.Controls.Add(Me.btnSmashTweet)
        Me.SmashBrosUltimatePage.Controls.Add(Me.btnSmashGenerate)
        Me.SmashBrosUltimatePage.Controls.Add(Me.SmashCharacterLabel)
        Me.SmashBrosUltimatePage.HorizontalScrollbarBarColor = True
        Me.SmashBrosUltimatePage.Location = New System.Drawing.Point(4, 35)
        Me.SmashBrosUltimatePage.Name = "SmashBrosUltimatePage"
        Me.SmashBrosUltimatePage.Size = New System.Drawing.Size(512, 159)
        Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Green
        Me.SmashBrosUltimatePage.TabIndex = 2
        Me.SmashBrosUltimatePage.Text = "Smash Bros. Ultimate"
        Me.SmashBrosUltimatePage.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.SmashBrosUltimatePage.VerticalScrollbarBarColor = True
        '
        'SmashStageName
        '
        Me.SmashStageName.AutoSize = True
        Me.SmashStageName.Location = New System.Drawing.Point(60, 26)
        Me.SmashStageName.Name = "SmashStageName"
        Me.SmashStageName.Size = New System.Drawing.Size(0, 0)
        Me.SmashStageName.Style = MetroFramework.MetroColorStyle.Green
        Me.SmashStageName.TabIndex = 31
        Me.SmashStageName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SmashStageLabel
        '
        Me.SmashStageLabel.AutoSize = True
        Me.SmashStageLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.SmashStageLabel.Location = New System.Drawing.Point(3, 26)
        Me.SmashStageLabel.Name = "SmashStageLabel"
        Me.SmashStageLabel.Size = New System.Drawing.Size(51, 19)
        Me.SmashStageLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.SmashStageLabel.TabIndex = 30
        Me.SmashStageLabel.Text = "Stage:"
        Me.SmashStageLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SmashCharacterName
        '
        Me.SmashCharacterName.AutoSize = True
        Me.SmashCharacterName.Location = New System.Drawing.Point(87, 7)
        Me.SmashCharacterName.Name = "SmashCharacterName"
        Me.SmashCharacterName.Size = New System.Drawing.Size(0, 0)
        Me.SmashCharacterName.Style = MetroFramework.MetroColorStyle.Green
        Me.SmashCharacterName.TabIndex = 29
        Me.SmashCharacterName.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'btnSmashTweet
        '
        Me.btnSmashTweet.Enabled = False
        Me.btnSmashTweet.Location = New System.Drawing.Point(259, 133)
        Me.btnSmashTweet.Name = "btnSmashTweet"
        Me.btnSmashTweet.Size = New System.Drawing.Size(250, 23)
        Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Green
        Me.btnSmashTweet.TabIndex = 28
        Me.btnSmashTweet.Text = "Tweet Results"
        Me.btnSmashTweet.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'btnSmashGenerate
        '
        Me.btnSmashGenerate.Location = New System.Drawing.Point(3, 133)
        Me.btnSmashGenerate.Name = "btnSmashGenerate"
        Me.btnSmashGenerate.Size = New System.Drawing.Size(250, 23)
        Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Green
        Me.btnSmashGenerate.TabIndex = 27
        Me.btnSmashGenerate.Text = "Generate"
        Me.btnSmashGenerate.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'SmashCharacterLabel
        '
        Me.SmashCharacterLabel.AutoSize = True
        Me.SmashCharacterLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.SmashCharacterLabel.Location = New System.Drawing.Point(3, 7)
        Me.SmashCharacterLabel.Name = "SmashCharacterLabel"
        Me.SmashCharacterLabel.Size = New System.Drawing.Size(78, 19)
        Me.SmashCharacterLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.SmashCharacterLabel.TabIndex = 9
        Me.SmashCharacterLabel.Text = "Character:"
        Me.SmashCharacterLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'AppSettings
        '
        Me.AppSettings.Controls.Add(Me.SettingsLocationLabel)
        Me.AppSettings.Controls.Add(Me.warnAboutNewVersion)
        Me.AppSettings.Controls.Add(Me.ColorLabel)
        Me.AppSettings.Controls.Add(Me.ThemeLabel)
        Me.AppSettings.Controls.Add(Me.ColorComboBox)
        Me.AppSettings.Controls.Add(Me.ThemeComboBox)
        Me.AppSettings.Controls.Add(Me.saveSettings)
        Me.AppSettings.HorizontalScrollbarBarColor = True
        Me.AppSettings.Location = New System.Drawing.Point(4, 35)
        Me.AppSettings.Name = "AppSettings"
        Me.AppSettings.Size = New System.Drawing.Size(512, 159)
        Me.AppSettings.TabIndex = 3
        Me.AppSettings.Text = "App Settings"
        Me.AppSettings.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.AppSettings.VerticalScrollbarBarColor = True
        '
        'SettingsLocationLabel
        '
        Me.SettingsLocationLabel.AutoSize = True
        Me.SettingsLocationLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.SettingsLocationLabel.Location = New System.Drawing.Point(5, 105)
        Me.SettingsLocationLabel.Name = "SettingsLocationLabel"
        Me.SettingsLocationLabel.Size = New System.Drawing.Size(150, 19)
        Me.SettingsLocationLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.SettingsLocationLabel.TabIndex = 10
        Me.SettingsLocationLabel.Text = "Settings Location Label"
        Me.SettingsLocationLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.SettingsLocationLabel.Visible = False
        '
        'warnAboutNewVersion
        '
        Me.warnAboutNewVersion.AutoSize = True
        Me.warnAboutNewVersion.Location = New System.Drawing.Point(304, 22)
        Me.warnAboutNewVersion.Name = "warnAboutNewVersion"
        Me.warnAboutNewVersion.Size = New System.Drawing.Size(208, 15)
        Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Green
        Me.warnAboutNewVersion.TabIndex = 32
        Me.warnAboutNewVersion.Text = "Warn about new version on startup"
        Me.warnAboutNewVersion.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.warnAboutNewVersion, "If new version is found it tells you on startup.")
        Me.warnAboutNewVersion.UseVisualStyleBackColor = True
        '
        'ColorLabel
        '
        Me.ColorLabel.AutoSize = True
        Me.ColorLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.ColorLabel.Location = New System.Drawing.Point(3, 51)
        Me.ColorLabel.Name = "ColorLabel"
        Me.ColorLabel.Size = New System.Drawing.Size(50, 19)
        Me.ColorLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.ColorLabel.TabIndex = 30
        Me.ColorLabel.Text = "Color:"
        Me.ColorLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'ThemeLabel
        '
        Me.ThemeLabel.AutoSize = True
        Me.ThemeLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.ThemeLabel.Location = New System.Drawing.Point(3, 0)
        Me.ThemeLabel.Name = "ThemeLabel"
        Me.ThemeLabel.Size = New System.Drawing.Size(58, 19)
        Me.ThemeLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.ThemeLabel.TabIndex = 28
        Me.ThemeLabel.Text = "Theme:"
        Me.ThemeLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'ColorComboBox
        '
        Me.ColorComboBox.FormattingEnabled = True
        Me.ColorComboBox.ItemHeight = 23
        Me.ColorComboBox.Items.AddRange(New Object() {"Black", "White", "Silver", "Blue", "Green", "Lime", "Teal", "Orange", "Brown", "Pink", "Magenta", "Purple", "Red", "Yellow"})
        Me.ColorComboBox.Location = New System.Drawing.Point(5, 73)
        Me.ColorComboBox.Name = "ColorComboBox"
        Me.ColorComboBox.Size = New System.Drawing.Size(164, 29)
        Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Green
        Me.ColorComboBox.TabIndex = 31
        Me.ColorComboBox.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'ThemeComboBox
        '
        Me.ThemeComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ThemeComboBox.FormattingEnabled = True
        Me.ThemeComboBox.ItemHeight = 23
        Me.ThemeComboBox.Items.AddRange(New Object() {"Dark", "Light"})
        Me.ThemeComboBox.Location = New System.Drawing.Point(5, 19)
        Me.ThemeComboBox.Name = "ThemeComboBox"
        Me.ThemeComboBox.Size = New System.Drawing.Size(164, 29)
        Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Green
        Me.ThemeComboBox.TabIndex = 29
        Me.ThemeComboBox.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'saveSettings
        '
        Me.saveSettings.AutoSize = True
        Me.saveSettings.Location = New System.Drawing.Point(304, 43)
        Me.saveSettings.Name = "saveSettings"
        Me.saveSettings.Size = New System.Drawing.Size(153, 15)
        Me.saveSettings.Style = MetroFramework.MetroColorStyle.Green
        Me.saveSettings.TabIndex = 28
        Me.saveSettings.Text = "Save application settings"
        Me.saveSettings.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.saveSettings, "Save the settings to remember everything after closing")
        Me.saveSettings.UseVisualStyleBackColor = True
        '
        'btnExitApplication
        '
        Me.btnExitApplication.Location = New System.Drawing.Point(413, 267)
        Me.btnExitApplication.Name = "btnExitApplication"
        Me.btnExitApplication.Size = New System.Drawing.Size(121, 23)
        Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Green
        Me.btnExitApplication.TabIndex = 6
        Me.btnExitApplication.Text = "Exit Application"
        Me.btnExitApplication.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.btnExitApplication, "Close the app")
        '
        'MetroToolTip1
        '
        Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Green
        Me.MetroToolTip1.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'btnAbout
        '
        Me.btnAbout.Location = New System.Drawing.Point(286, 267)
        Me.btnAbout.Name = "btnAbout"
        Me.btnAbout.Size = New System.Drawing.Size(121, 23)
        Me.btnAbout.Style = MetroFramework.MetroColorStyle.Green
        Me.btnAbout.TabIndex = 7
        Me.btnAbout.Text = "About Application"
        Me.btnAbout.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.btnAbout, "Info about the app")
        '
        'btnDiscord
        '
        Me.btnDiscord.Location = New System.Drawing.Point(159, 267)
        Me.btnDiscord.Name = "btnDiscord"
        Me.btnDiscord.Size = New System.Drawing.Size(121, 23)
        Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Green
        Me.btnDiscord.TabIndex = 8
        Me.btnDiscord.Text = "Discord"
        Me.btnDiscord.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.btnDiscord, "Join Cody's Discord server")
        '
        'btnTwitter
        '
        Me.btnTwitter.Location = New System.Drawing.Point(32, 267)
        Me.btnTwitter.Name = "btnTwitter"
        Me.btnTwitter.Size = New System.Drawing.Size(121, 23)
        Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Green
        Me.btnTwitter.TabIndex = 9
        Me.btnTwitter.Text = "Twitter"
        Me.btnTwitter.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroToolTip1.SetToolTip(Me.btnTwitter, "Visit Cody's Twitter")
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle
        Me.ClientSize = New System.Drawing.Size(566, 299)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnTwitter)
        Me.Controls.Add(Me.btnDiscord)
        Me.Controls.Add(Me.btnAbout)
        Me.Controls.Add(Me.btnExitApplication)
        Me.Controls.Add(Me.MetroTabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "MainForm"
        Me.Resizable = False
        Me.Style = MetroFramework.MetroColorStyle.Green
        Me.Text = "Nintendo Game Randomizer"
        Me.TextAlign = System.Windows.Forms.VisualStyles.HorizontalAlign.Center
        Me.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MK8DeluxePage.ResumeLayout(False)
        Me.MK8DeluxePage.PerformLayout()
        Me.Splatoon2Page.ResumeLayout(False)
        Me.Splatoon2Page.PerformLayout()
        Me.SmashBrosUltimatePage.ResumeLayout(False)
        Me.SmashBrosUltimatePage.PerformLayout()
        Me.AppSettings.ResumeLayout(False)
        Me.AppSettings.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MK8DeluxePage As MetroFramework.Controls.MetroTabPage
    Friend WithEvents Splatoon2Page As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MK8TrackLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MK8VehicleLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MK8CharacterLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents btnMK8Generate As MetroFramework.Controls.MetroButton
    Friend WithEvents btnExitApplication As MetroFramework.Controls.MetroButton
    Friend WithEvents MK8GliderLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MK8TiresLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SmashBrosUltimatePage As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MK8TrackName As MetroFramework.Controls.MetroLabel
    Friend WithEvents MK8GliderName As MetroFramework.Controls.MetroLabel
    Friend WithEvents MK8TiresName As MetroFramework.Controls.MetroLabel
    Friend WithEvents MK8VehicleName As MetroFramework.Controls.MetroLabel
    Friend WithEvents MK8CharacterName As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroToolTip1 As MetroFramework.Components.MetroToolTip
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MiiNameLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MiiNameTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents checkCustomMiiNames As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents SmashCharacterLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents checkOnlyCharAndParts As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents checkBattleMode As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents MK8BattleModeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MK8BattleModeName As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatModeName As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatStageName As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatShoesName As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatClothingName As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatHeadgearName As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatWeaponName As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatModeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatShoesLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatClothingLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatStageLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatWeaponLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SplatHeadgearLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents btnSplatGenerate As MetroFramework.Controls.MetroButton
    Friend WithEvents checkOnlyWeaponClothes As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents btnAbout As MetroFramework.Controls.MetroButton
    Friend WithEvents btnDiscord As MetroFramework.Controls.MetroButton
    Friend WithEvents btnTwitter As MetroFramework.Controls.MetroButton
    Friend WithEvents checkOnlySplatlings As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents checkOnlyBrellas As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents checkOnlyDuelies As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents checkOnlyChargers As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents checkOnlyRollers As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents checkOnlyBlasters As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents checkOnlyShooters As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents checkAllWeapons As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents checkOnlySloshers As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents btnMK8Tweet As MetroFramework.Controls.MetroButton
    Friend WithEvents btnSplatTweet As MetroFramework.Controls.MetroButton
    Friend WithEvents checkExcludeGold As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents saveSettings As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents AppSettings As MetroFramework.Controls.MetroTabPage
    Friend WithEvents ColorComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents ColorLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents ThemeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents ThemeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents warnAboutNewVersion As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents btnSmashTweet As MetroFramework.Controls.MetroButton
    Friend WithEvents btnSmashGenerate As MetroFramework.Controls.MetroButton
    Friend WithEvents SmashCharacterName As MetroFramework.Controls.MetroLabel
    Friend WithEvents SmashStageName As MetroFramework.Controls.MetroLabel
    Friend WithEvents SmashStageLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SettingsLocationLabel As MetroFramework.Controls.MetroLabel
End Class
