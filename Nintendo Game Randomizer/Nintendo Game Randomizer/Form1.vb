﻿Public Class MainForm
    Private Sub btnExitApplication_Click(sender As Object, e As EventArgs) Handles btnExitApplication.Click
        Application.Exit()
    End Sub

    'Mario Kart 8 Deluxe tab'

    Private Sub btnMK8Generate_Click(sender As Object, e As EventArgs) Handles btnMK8Generate.Click
        Dim MK8CharacterNumber As Integer = 44
        Dim MK8VehicleNumber As Integer = 42
        Dim MK8TiresNumber As Integer = 23
        Dim MK8GliderNumber As Integer = 16
        Dim MK8TrackNumber As Integer = 25
        Dim MK8BattleStageNumber As Integer = 9
        Dim MK8BattleModeNumber As Integer = 6
        Dim r As New Random
        Dim RandomMK8Character
        Dim RandomMK8Vehicle
        Dim RandomMK8Tires
        Dim RandomMK8Glider
        Dim RandomMK8Track
        Dim RandomMK8BattleStage
        Dim RandomMK8BattleMode
        RandomMK8Character = r.Next(MK8CharacterNumber)
        RandomMK8Vehicle = r.Next(MK8VehicleNumber)
        RandomMK8Tires = r.Next(MK8TiresNumber)
        RandomMK8Glider = r.Next(MK8GliderNumber)
        RandomMK8Track = r.Next(MK8TrackNumber)
        RandomMK8BattleStage = r.Next(MK8BattleStageNumber)
        RandomMK8BattleMode = r.Next(MK8BattleModeNumber)
        Dim MK8ExcludedCharacterNumber As Integer = 43
        Dim MK8ExcludedVehicleNumber As Integer = 41
        Dim MK8ExcludedTiresNumber As Integer = 22
        Dim MK8ExcludedGliderNumber As Integer = 15
        Dim RandomExcludedMK8Character
        Dim RandomExcludedMK8Vehicle
        Dim RandomExcludedMK8Tires
        Dim RandomExcludedMK8Glider
        RandomExcludedMK8Character = r.Next(MK8ExcludedCharacterNumber)
        RandomExcludedMK8Vehicle = r.Next(MK8ExcludedVehicleNumber)
        RandomExcludedMK8Tires = r.Next(MK8ExcludedTiresNumber)
        RandomExcludedMK8Glider = r.Next(MK8ExcludedGliderNumber)

        If checkCustomMiiNames.Checked = True And MiiNameTextBox.Text = "" Then
            MsgBox("You have Mii Names enabled please enter a name first before generating the results!", MsgBoxStyle.Exclamation)
        Else
            If checkExcludeGold.Checked = False Then
                Select Case RandomMK8Character
                    Case 1
                        MK8CharacterName.Text = "Baby Daisy"
                    Case 2
                        MK8CharacterName.Text = "Baby Luigi"
                    Case 3
                        MK8CharacterName.Text = "Baby Mario"
                    Case 4
                        MK8CharacterName.Text = "Baby Peach"
                    Case 5
                        MK8CharacterName.Text = "Baby Rosalina"
                    Case 6
                        MK8CharacterName.Text = "Lemmy"
                    Case 7
                        MK8CharacterName.Text = "Dry Bones"
                    Case 8
                        MK8CharacterName.Text = "Koopa Troopa"
                    Case 9
                        MK8CharacterName.Text = "Lakitu"
                    Case 10
                        MK8CharacterName.Text = "Larry"
                    Case 11
                        MK8CharacterName.Text = "Toad"
                    Case 12
                        MK8CharacterName.Text = "Toadette"
                    Case 13
                        MK8CharacterName.Text = "Wendy"
                    Case 14
                        MK8CharacterName.Text = "Isabelle"
                    Case 15
                        MK8CharacterName.Text = "Bowser Jr."
                    Case 16
                        MK8CharacterName.Text = "Princess Daisy"
                    Case 17
                        MK8CharacterName.Text = "Princess Peach"
                    Case 18
                        MK8CharacterName.Text = "Yoshi"
                    Case 19
                        MK8CharacterName.Text = "Tanooki Mario"
                    Case 20
                        MK8CharacterName.Text = "Cat Peach"
                    Case 21
                        MK8CharacterName.Text = "Villager"
                    Case 22
                        MK8CharacterName.Text = "Inkling Boy"
                    Case 23
                        MK8CharacterName.Text = "Inkling Girl"
                    Case 24
                        MK8CharacterName.Text = "Mario"
                    Case 25
                        MK8CharacterName.Text = "Luigi"
                    Case 26
                        MK8CharacterName.Text = "Ludwig"
                    Case 27
                        MK8CharacterName.Text = "Iggy"
                    Case 28
                        MK8CharacterName.Text = "Donkey Kong"
                    Case 29
                        MK8CharacterName.Text = "Metal Mario"
                    Case 30
                        MK8CharacterName.Text = "Pink Gold Peach"
                    Case 31
                        MK8CharacterName.Text = "Rosalina"
                    Case 32
                        MK8CharacterName.Text = "Roy"
                    Case 33
                        MK8CharacterName.Text = "Waluigi"
                    Case 34
                        MK8CharacterName.Text = "Link"
                    Case 35
                        MK8CharacterName.Text = "King Boo"
                    Case 36
                        MK8CharacterName.Text = "Gold Mario"
                    Case 37
                        MK8CharacterName.Text = "Bowser"
                    Case 38
                        MK8CharacterName.Text = "Wario"
                    Case 39
                        MK8CharacterName.Text = "Morton"
                    Case 40
                        MK8CharacterName.Text = "Dry Bowser"
                    Case 41
                        If checkCustomMiiNames.Checked = True Then
                            MK8CharacterName.Text = "Small Mii " + "(" + MiiNameTextBox.Text + ")"
                        Else
                            MK8CharacterName.Text = "Small Mii"
                        End If
                    Case 42
                        If checkCustomMiiNames.Checked = True Then
                            MK8CharacterName.Text = "Medium Mii " + "(" + MiiNameTextBox.Text + ")"
                        Else
                            MK8CharacterName.Text = "Medium Mii"
                        End If
                    Case 43
                        If checkCustomMiiNames.Checked = True Then
                            MK8CharacterName.Text = "Large Mii " + "(" + MiiNameTextBox.Text + ")"
                        Else
                            MK8CharacterName.Text = "Large Mii"
                        End If
                End Select

                Select Case RandomMK8Vehicle
                    Case 1
                        MK8VehicleName.Text = "Standard Kart"
                    Case 2
                        MK8VehicleName.Text = "Pipe Frame"
                    Case 3
                        MK8VehicleName.Text = "Mach 8"
                    Case 4
                        MK8VehicleName.Text = "Steel Driver"
                    Case 5
                        MK8VehicleName.Text = "Cat Cruiser"
                    Case 6
                        MK8VehicleName.Text = "Circuit Special"
                    Case 7
                        MK8VehicleName.Text = "Tri-Speeder"
                    Case 8
                        MK8VehicleName.Text = "Badwagon"
                    Case 9
                        MK8VehicleName.Text = "Prancer"
                    Case 10
                        MK8VehicleName.Text = "Biddybuggy"
                    Case 11
                        MK8VehicleName.Text = "Landship"
                    Case 12
                        MK8VehicleName.Text = "Sneeker"
                    Case 13
                        MK8VehicleName.Text = "Sports Coupe"
                    Case 14
                        MK8VehicleName.Text = "Gold Standard"
                    Case 15
                        MK8VehicleName.Text = "GLA"
                    Case 16
                        MK8VehicleName.Text = "W 25 Silver Arrow"
                    Case 17
                        MK8VehicleName.Text = "300 SL Roadster"
                    Case 18
                        MK8VehicleName.Text = "Blue Falcon"
                    Case 19
                        MK8VehicleName.Text = "Tanooki Kart"
                    Case 20
                        MK8VehicleName.Text = "B Dasher"
                    Case 21
                        MK8VehicleName.Text = "Streetle"
                    Case 22
                        MK8VehicleName.Text = "P-Wing"
                    Case 23
                        MK8VehicleName.Text = "Koopa Clown"
                    Case 24
                        MK8VehicleName.Text = "Standard Bike"
                    Case 25
                        MK8VehicleName.Text = "The Duke"
                    Case 26
                        MK8VehicleName.Text = "Flame Rider"
                    Case 27
                        MK8VehicleName.Text = "Varmint"
                    Case 28
                        MK8VehicleName.Text = "Mr. Scooty"
                    Case 29
                        MK8VehicleName.Text = "City Tripper"
                    Case 30
                        MK8VehicleName.Text = "Comet"
                    Case 31
                        MK8VehicleName.Text = "Sport Bike"
                    Case 32
                        MK8VehicleName.Text = "Jet Bike"
                    Case 33
                        MK8VehicleName.Text = "Yoshi Bike"
                    Case 34
                        MK8VehicleName.Text = "Master Cycle"
                    Case 35
                        MK8VehicleName.Text = "Standard ATV"
                    Case 36
                        MK8VehicleName.Text = "Wild Wiggler"
                    Case 37
                        MK8VehicleName.Text = "Teddy Buggy"
                    Case 38
                        MK8VehicleName.Text = "Bone Rattler"
                    Case 39
                        MK8VehicleName.Text = "Splat Buggy"
                    Case 40
                        MK8VehicleName.Text = "Inkstriker"
                    Case 41
                        MK8VehicleName.Text = "Master Cycle Zero"
                End Select

                Select Case RandomMK8Tires
                    Case 1
                        MK8TiresName.Text = "Standard"
                    Case 2
                        MK8TiresName.Text = "Monster"
                    Case 3
                        MK8TiresName.Text = "Roller"
                    Case 4
                        MK8TiresName.Text = "Slim"
                    Case 5
                        MK8TiresName.Text = "Slick"
                    Case 6
                        MK8TiresName.Text = "Metal"
                    Case 7
                        MK8TiresName.Text = "Button"
                    Case 8
                        MK8TiresName.Text = "Off-Road"
                    Case 9
                        MK8TiresName.Text = "Sponge"
                    Case 10
                        MK8TiresName.Text = "Wood"
                    Case 11
                        MK8TiresName.Text = "Cushion"
                    Case 12
                        MK8TiresName.Text = "Blue Standard"
                    Case 13
                        MK8TiresName.Text = "Hot Monster"
                    Case 14
                        MK8TiresName.Text = "Azure Roller"
                    Case 15
                        MK8TiresName.Text = "Crimson Slim"
                    Case 16
                        MK8TiresName.Text = "Cyber Slick"
                    Case 17
                        MK8TiresName.Text = "Retro Off-Road"
                    Case 18
                        MK8TiresName.Text = "Gold Tires"
                    Case 19
                        MK8TiresName.Text = "GLA Tires"
                    Case 20
                        MK8TiresName.Text = "Triforce Tires"
                    Case 21
                        MK8TiresName.Text = "Leaf Tires"
                    Case 22
                        MK8TiresName.Text = "Anicient Tires"
                End Select

                Select Case RandomMK8Glider
                    Case 1
                        MK8GliderName.Text = "Super Glider"
                    Case 2
                        MK8GliderName.Text = "Cloud Glider"
                    Case 3
                        MK8GliderName.Text = "Wario Wing"
                    Case 4
                        MK8GliderName.Text = "Waddle Wing"
                    Case 5
                        MK8GliderName.Text = "Peach Parasol"
                    Case 6
                        MK8GliderName.Text = "Parachute"
                    Case 7
                        MK8GliderName.Text = "Parafoil"
                    Case 8
                        MK8GliderName.Text = "Flower Glider"
                    Case 9
                        MK8GliderName.Text = "Bowser Kite"
                    Case 10
                        MK8GliderName.Text = "Plane Glider"
                    Case 11
                        MK8GliderName.Text = "MKTV Parafoil"
                    Case 12
                        MK8GliderName.Text = "Gold Glider"
                    Case 13
                        MK8GliderName.Text = "Hylian Kite"
                    Case 14
                        MK8GliderName.Text = "Paper Glider"
                    Case 15
                        MK8GliderName.Text = "Paraglider"
                End Select
            Else
                Select Case RandomExcludedMK8Character
                    Case 1
                        MK8CharacterName.Text = "Baby Daisy"
                    Case 2
                        MK8CharacterName.Text = "Baby Luigi"
                    Case 3
                        MK8CharacterName.Text = "Baby Mario"
                    Case 4
                        MK8CharacterName.Text = "Baby Peach"
                    Case 5
                        MK8CharacterName.Text = "Baby Rosalina"
                    Case 6
                        MK8CharacterName.Text = "Lemmy"
                    Case 7
                        MK8CharacterName.Text = "Dry Bones"
                    Case 8
                        MK8CharacterName.Text = "Koopa Troopa"
                    Case 9
                        MK8CharacterName.Text = "Lakitu"
                    Case 10
                        MK8CharacterName.Text = "Larry"
                    Case 11
                        MK8CharacterName.Text = "Toad"
                    Case 12
                        MK8CharacterName.Text = "Toadette"
                    Case 13
                        MK8CharacterName.Text = "Wendy"
                    Case 14
                        MK8CharacterName.Text = "Isabelle"
                    Case 15
                        MK8CharacterName.Text = "Bowser Jr."
                    Case 16
                        MK8CharacterName.Text = "Princess Daisy"
                    Case 17
                        MK8CharacterName.Text = "Princess Peach"
                    Case 18
                        MK8CharacterName.Text = "Yoshi"
                    Case 19
                        MK8CharacterName.Text = "Tanooki Mario"
                    Case 20
                        MK8CharacterName.Text = "Cat Peach"
                    Case 21
                        MK8CharacterName.Text = "Villager"
                    Case 22
                        MK8CharacterName.Text = "Inkling Boy"
                    Case 23
                        MK8CharacterName.Text = "Inkling Girl"
                    Case 24
                        MK8CharacterName.Text = "Mario"
                    Case 25
                        MK8CharacterName.Text = "Luigi"
                    Case 26
                        MK8CharacterName.Text = "Ludwig"
                    Case 27
                        MK8CharacterName.Text = "Iggy"
                    Case 28
                        MK8CharacterName.Text = "Donkey Kong"
                    Case 29
                        MK8CharacterName.Text = "Metal Mario"
                    Case 30
                        MK8CharacterName.Text = "Pink Gold Peach"
                    Case 31
                        MK8CharacterName.Text = "Rosalina"
                    Case 32
                        MK8CharacterName.Text = "Roy"
                    Case 33
                        MK8CharacterName.Text = "Waluigi"
                    Case 34
                        MK8CharacterName.Text = "Link"
                    Case 35
                        MK8CharacterName.Text = "King Boo"
                    Case 36
                        MK8CharacterName.Text = "Bowser"
                    Case 37
                        MK8CharacterName.Text = "Wario"
                    Case 38
                        MK8CharacterName.Text = "Morton"
                    Case 39
                        MK8CharacterName.Text = "Dry Bowser"
                    Case 40
                        If checkCustomMiiNames.Checked = True Then
                            MK8CharacterName.Text = "Small Mii " + "(" + MiiNameTextBox.Text + ")"
                        Else
                            MK8CharacterName.Text = "Small Mii"
                        End If
                    Case 41
                        If checkCustomMiiNames.Checked = True Then
                            MK8CharacterName.Text = "Medium Mii " + "(" + MiiNameTextBox.Text + ")"
                        Else
                            MK8CharacterName.Text = "Medium Mii"
                        End If
                    Case 42
                        If checkCustomMiiNames.Checked = True Then
                            MK8CharacterName.Text = "Large Mii " + "(" + MiiNameTextBox.Text + ")"
                        Else
                            MK8CharacterName.Text = "Large Mii"
                        End If
                End Select

                Select Case RandomExcludedMK8Vehicle
                    Case 1
                        MK8VehicleName.Text = "Standard Kart"
                    Case 2
                        MK8VehicleName.Text = "Pipe Frame"
                    Case 3
                        MK8VehicleName.Text = "Mach 8"
                    Case 4
                        MK8VehicleName.Text = "Steel Driver"
                    Case 5
                        MK8VehicleName.Text = "Cat Cruiser"
                    Case 6
                        MK8VehicleName.Text = "Circuit Special"
                    Case 7
                        MK8VehicleName.Text = "Tri-Speeder"
                    Case 8
                        MK8VehicleName.Text = "Badwagon"
                    Case 9
                        MK8VehicleName.Text = "Prancer"
                    Case 10
                        MK8VehicleName.Text = "Biddybuggy"
                    Case 11
                        MK8VehicleName.Text = "Landship"
                    Case 12
                        MK8VehicleName.Text = "Sneeker"
                    Case 13
                        MK8VehicleName.Text = "Sports Coupe"
                    Case 14
                        MK8VehicleName.Text = "GLA"
                    Case 15
                        MK8VehicleName.Text = "W 25 Silver Arrow"
                    Case 16
                        MK8VehicleName.Text = "300 SL Roadster"
                    Case 17
                        MK8VehicleName.Text = "Blue Falcon"
                    Case 18
                        MK8VehicleName.Text = "Tanooki Kart"
                    Case 19
                        MK8VehicleName.Text = "B Dasher"
                    Case 20
                        MK8VehicleName.Text = "Streetle"
                    Case 21
                        MK8VehicleName.Text = "P-Wing"
                    Case 22
                        MK8VehicleName.Text = "Koopa Clown"
                    Case 23
                        MK8VehicleName.Text = "Standard Bike"
                    Case 24
                        MK8VehicleName.Text = "The Duke"
                    Case 25
                        MK8VehicleName.Text = "Flame Rider"
                    Case 26
                        MK8VehicleName.Text = "Varmint"
                    Case 27
                        MK8VehicleName.Text = "Mr. Scooty"
                    Case 28
                        MK8VehicleName.Text = "City Tripper"
                    Case 29
                        MK8VehicleName.Text = "Comet"
                    Case 30
                        MK8VehicleName.Text = "Sport Bike"
                    Case 31
                        MK8VehicleName.Text = "Jet Bike"
                    Case 32
                        MK8VehicleName.Text = "Yoshi Bike"
                    Case 33
                        MK8VehicleName.Text = "Master Cycle"
                    Case 34
                        MK8VehicleName.Text = "Standard ATV"
                    Case 35
                        MK8VehicleName.Text = "Wild Wiggler"
                    Case 36
                        MK8VehicleName.Text = "Teddy Buggy"
                    Case 37
                        MK8VehicleName.Text = "Bone Rattler"
                    Case 38
                        MK8VehicleName.Text = "Splat Buggy"
                    Case 39
                        MK8VehicleName.Text = "Inkstriker"
                    Case 40
                        MK8VehicleName.Text = "Master Cycle Zero"
                End Select

                Select Case RandomExcludedMK8Tires
                    Case 1
                        MK8TiresName.Text = "Standard"
                    Case 2
                        MK8TiresName.Text = "Monster"
                    Case 3
                        MK8TiresName.Text = "Roller"
                    Case 4
                        MK8TiresName.Text = "Slim"
                    Case 5
                        MK8TiresName.Text = "Slick"
                    Case 6
                        MK8TiresName.Text = "Metal"
                    Case 7
                        MK8TiresName.Text = "Button"
                    Case 8
                        MK8TiresName.Text = "Off-Road"
                    Case 9
                        MK8TiresName.Text = "Sponge"
                    Case 10
                        MK8TiresName.Text = "Wood"
                    Case 11
                        MK8TiresName.Text = "Cushion"
                    Case 12
                        MK8TiresName.Text = "Blue Standard"
                    Case 13
                        MK8TiresName.Text = "Hot Monster"
                    Case 14
                        MK8TiresName.Text = "Azure Roller"
                    Case 15
                        MK8TiresName.Text = "Crimson Slim"
                    Case 16
                        MK8TiresName.Text = "Cyber Slick"
                    Case 17
                        MK8TiresName.Text = "Retro Off-Road"
                    Case 18
                        MK8TiresName.Text = "GLA Tires"
                    Case 19
                        MK8TiresName.Text = "Triforce Tires"
                    Case 20
                        MK8TiresName.Text = "Leaf Tires"
                    Case 21
                        MK8TiresName.Text = "Anicient Tires"
                End Select

                Select Case RandomExcludedMK8Glider
                    Case 1
                        MK8GliderName.Text = "Super Glider"
                    Case 2
                        MK8GliderName.Text = "Cloud Glider"
                    Case 3
                        MK8GliderName.Text = "Wario Wing"
                    Case 4
                        MK8GliderName.Text = "Waddle Wing"
                    Case 5
                        MK8GliderName.Text = "Peach Parasol"
                    Case 6
                        MK8GliderName.Text = "Parachute"
                    Case 7
                        MK8GliderName.Text = "Parafoil"
                    Case 8
                        MK8GliderName.Text = "Flower Glider"
                    Case 9
                        MK8GliderName.Text = "Bowser Kite"
                    Case 10
                        MK8GliderName.Text = "Plane Glider"
                    Case 11
                        MK8GliderName.Text = "MKTV Parafoil"
                    Case 12
                        MK8GliderName.Text = "Hylian Kite"
                    Case 13
                        MK8GliderName.Text = "Paper Glider"
                    Case 14
                        MK8GliderName.Text = "Paraglider"
                End Select
            End If

            If checkOnlyCharAndParts.Checked = True Then
                MK8TrackLabel.Visible = False
                MK8TrackName.Visible = False
                MK8BattleModeLabel.Visible = False
                MK8BattleModeName.Visible = False
            Else
                MK8TrackLabel.Visible = True
                MK8TrackName.Visible = True
                MK8BattleModeLabel.Visible = True
                MK8BattleModeName.Visible = True

                Select Case RandomMK8Track
                    Case 1
                        MK8TrackName.Text = "Mario Kart Stadium"
                    Case 2
                        MK8TrackName.Text = "Water Park"
                    Case 3
                        MK8TrackName.Text = "Sweet Sweet Canyon"
                    Case 4
                        MK8TrackName.Text = "Thwomp Ruins"
                    Case 5
                        MK8TrackName.Text = "Mario Circuit"
                    Case 6
                        MK8TrackName.Text = "Toad Harbor"
                    Case 7
                        MK8TrackName.Text = "Twisted Mansion"
                    Case 8
                        MK8TrackName.Text = "Shy Guy Falls"
                    Case 9
                        MK8TrackName.Text = "[Wii] Moo Moo Meadows"
                    Case 10
                        MK8TrackName.Text = "[GBA] Mario Circuit"
                    Case 11
                        MK8TrackName.Text = "[DS] Cheep Cheep Beach"
                    Case 12
                        MK8TrackName.Text = "[N64] Toad's Turnpike"
                    Case 13
                        MK8TrackName.Text = "[GCN] Dry Dry Desert"
                    Case 14
                        MK8TrackName.Text = "[SNES] Donut Plains 3"
                    Case 15
                        MK8TrackName.Text = "[N64] Royal Raceway"
                    Case 16
                        MK8TrackName.Text = "[3DS] DK Jungle"
                    Case 17
                        MK8TrackName.Text = "[GCN] Yoshi Circuit"
                    Case 18
                        MK8TrackName.Text = "Excitebike Arena"
                    Case 19
                        MK8TrackName.Text = "Dragon Driftway"
                    Case 20
                        MK8TrackName.Text = "Mute City"
                    Case 21
                        MK8TrackName.Text = "[Wii] Wario's Gold Mine"
                    Case 22
                        MK8TrackName.Text = "[SNES] Rainbow Road"
                    Case 23
                        MK8TrackName.Text = "Ice Ice Outpost"
                    Case 24
                        MK8TrackName.Text = "Hyrule Circuit"
                End Select
            End If

            If checkBattleMode.Checked = True Then
                MK8TrackLabel.Text = "Course:"
                MK8BattleModeLabel.Text = "Mode:"

                Select Case RandomMK8BattleStage
                    Case 1
                        MK8TrackName.Text = "Battle Stadium"
                    Case 2
                        MK8TrackName.Text = "Sweet Sweet Kingdom"
                    Case 3
                        MK8TrackName.Text = "Dragon Palace"
                    Case 4
                        MK8TrackName.Text = "Lunar Colony"
                    Case 5
                        MK8TrackName.Text = "[3DS] Wuhu Town"
                    Case 6
                        MK8TrackName.Text = "[GCN] Luigi's Mansion"
                    Case 7
                        MK8TrackName.Text = "[SNES] Battle Course 1"
                    Case 8
                        MK8TrackName.Text = "Urchin Underpass"
                End Select

                Select Case RandomMK8BattleMode
                    Case 1
                        MK8BattleModeName.Text = "Balloon Battle"
                    Case 2
                        MK8BattleModeName.Text = "Renegade Roundup"
                    Case 3
                        MK8BattleModeName.Text = "Bob-omb Blast"
                    Case 4
                        MK8BattleModeName.Text = "Coin Runners"
                    Case 5
                        MK8BattleModeName.Text = "Shine Thief"
                End Select
            End If
        End If

        If checkBattleMode.Checked = False Then
            MK8TrackLabel.Text = "Track:"
            MK8BattleModeLabel.ResetText()
            MK8BattleModeName.ResetText()
        End If

        btnMK8Tweet.Enabled = True
    End Sub

    Private Sub checkCustomMiiNames_CheckedChanged(sender As Object, e As EventArgs) Handles checkCustomMiiNames.CheckedChanged
        If checkCustomMiiNames.Checked = True Then
            MiiNameTextBox.Enabled = True
            MiiNameTextBox.Visible = True
            MiiNameLabel.Visible = True
            checkCustomMiiNames.Location = New System.Drawing.Point(309, 51)
            checkBattleMode.Location = New System.Drawing.Point(309, 72)
            checkOnlyCharAndParts.Location = New System.Drawing.Point(309, 93)
            checkExcludeGold.Location = New System.Drawing.Point(309, 114)
        Else
            MiiNameTextBox.Enabled = False
            MiiNameTextBox.Visible = False
            MiiNameLabel.Visible = False
            checkCustomMiiNames.Location = New System.Drawing.Point(309, 11)
            checkBattleMode.Location = New System.Drawing.Point(309, 30)
            checkOnlyCharAndParts.Location = New System.Drawing.Point(309, 49)
            checkExcludeGold.Location = New System.Drawing.Point(309, 68)
        End If
    End Sub

    Private Sub checkOnlyCharAndParts_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlyCharAndParts.CheckedChanged
        If checkOnlyCharAndParts.Checked = True Then
            checkBattleMode.Checked = False
            checkBattleMode.Enabled = False
        Else
            checkBattleMode.Enabled = True
        End If

        btnMK8Tweet.Enabled = False
    End Sub

    'Splatoon 2 tab'

    Private Sub btnSplatGenerate_Click(sender As Object, e As EventArgs) Handles btnSplatGenerate.Click
        Dim SplatAllWeaponNumber As Integer = 112
        Dim SplatShooterWeaponNumber As Integer = 30
        Dim SplatBlasterWeaponNumber As Integer = 14
        Dim SplatRollerWeaponNumber As Integer = 16
        Dim SplatChargerWeaponNumber As Integer = 18
        Dim SplatSlosherWeaponNumber As Integer = 10
        Dim SplatSplatlingWeaponNumber As Integer = 10
        Dim SplatDuelieWeaponNumber As Integer = 13
        Dim SplatBrellaWeaponNumber As Integer = 8
        Dim SplatHeadgearNumber As Integer = 132
        Dim SplatClothingNumber As Integer = 245
        Dim SplatShoesNumber As Integer = 150
        Dim SplatStageNumber As Integer = 22
        Dim SplatModeNumber As Integer = 6
        Dim r As New Random
        Dim RandomSplatAllWeapon
        Dim RandomSplatShooterWeapon
        Dim RandomSplatBlasterWeapon
        Dim RandomSplatRollerWeapon
        Dim RandomSplatChargerWeapon
        Dim RandomSplatSlosherWeapon
        Dim RandomSplatSplatlingWeapon
        Dim RandomSplatDuelieWeapon
        Dim RandomSplatBrellaWeapon
        Dim RandomSplatStage
        Dim RandomSplatMode
        Dim RandomSplatHeadgear
        Dim RandomSplatClothing
        Dim RandomSplatShoes
        RandomSplatAllWeapon = r.Next(SplatAllWeaponNumber)
        RandomSplatShooterWeapon = r.Next(SplatShooterWeaponNumber)
        RandomSplatBlasterWeapon = r.Next(SplatBlasterWeaponNumber)
        RandomSplatRollerWeapon = r.Next(SplatRollerWeaponNumber)
        RandomSplatChargerWeapon = r.Next(SplatChargerWeaponNumber)
        RandomSplatSlosherWeapon = r.Next(SplatSlosherWeaponNumber)
        RandomSplatSplatlingWeapon = r.Next(SplatSplatlingWeaponNumber)
        RandomSplatDuelieWeapon = r.Next(SplatDuelieWeaponNumber)
        RandomSplatBrellaWeapon = r.Next(SplatBrellaWeaponNumber)
        RandomSplatHeadgear = r.Next(SplatHeadgearNumber)
        RandomSplatClothing = r.Next(SplatClothingNumber)
        RandomSplatShoes = r.Next(SplatShoesNumber)
        RandomSplatStage = r.Next(SplatStageNumber)
        RandomSplatMode = r.Next(SplatModeNumber)

        If checkAllWeapons.Checked = False And checkOnlyShooters.Checked = False And checkOnlyBlasters.Checked = False And checkOnlyRollers.Checked = False And checkOnlySloshers.Checked = False And checkOnlyChargers.Checked = False And checkOnlySplatlings.Checked = False And checkOnlyDuelies.Checked = False And checkOnlyBrellas.Checked = False Then
            MsgBox("Please choose one of the following All Weapons,Only Shooters,Only Blasters,Only Rollers,Only Sloshers,Only Chargers,Only Splatlings,Only Duelies, or Only Brellas to be able to generate results!", MsgBoxStyle.Exclamation)
        Else
            If checkAllWeapons.Checked = True Then
                Select Case RandomSplatAllWeapon
                    Case 1
                        SplatWeaponName.Text = "Sploosh-o-matic"
                    Case 2
                        SplatWeaponName.Text = "Neo Sploosh-o-matic"
                    Case 3
                        SplatWeaponName.Text = "Splattershot Jr."
                    Case 4
                        SplatWeaponName.Text = "Custom Splattershot Jr."
                    Case 5
                        SplatWeaponName.Text = "Splash-o-matic"
                    Case 6
                        SplatWeaponName.Text = "Aerospray MG"
                    Case 7
                        SplatWeaponName.Text = "Aerospray RG"
                    Case 8
                        SplatWeaponName.Text = "Splattershot"
                    Case 9
                        SplatWeaponName.Text = "Tentatek Splattershot"
                    Case 10
                        SplatWeaponName.Text = "Hero Shot Replica"
                    Case 11
                        SplatWeaponName.Text = ".52 Gal"
                    Case 12
                        SplatWeaponName.Text = ".52 Gal Deco"
                    Case 13
                        SplatWeaponName.Text = "N-ZAP '85"
                    Case 14
                        SplatWeaponName.Text = "N-ZAP '89"
                    Case 15
                        SplatWeaponName.Text = "Splattershot Pro"
                    Case 16
                        SplatWeaponName.Text = "Forge Splattershot Pro"
                    Case 17
                        SplatWeaponName.Text = ".96 Gal"
                    Case 18
                        SplatWeaponName.Text = ".96 Gal Deco"
                    Case 19
                        SplatWeaponName.Text = "Jet Squelcher"
                    Case 20
                        SplatWeaponName.Text = "Custom Jet Squelcher"
                    Case 21
                        SplatWeaponName.Text = "Luna Blaster"
                    Case 22
                        SplatWeaponName.Text = "Luna Blaster Neo"
                    Case 23
                        SplatWeaponName.Text = "Blaster"
                    Case 24
                        SplatWeaponName.Text = "Custom Blaster"
                    Case 25
                        SplatWeaponName.Text = "Hero Blaster Replica"
                    Case 26
                        SplatWeaponName.Text = "Range Blaster"
                    Case 27
                        SplatWeaponName.Text = "Clash Blaster"
                    Case 28
                        SplatWeaponName.Text = "Clash Blaster Neo"
                    Case 29
                        SplatWeaponName.Text = "Rapid Blaster"
                    Case 30
                        SplatWeaponName.Text = "Rapid Blaster Deco"
                    Case 31
                        SplatWeaponName.Text = "Rapid Blaster Pro"
                    Case 32
                        SplatWeaponName.Text = "L-3 Nozzlenose"
                    Case 33
                        SplatWeaponName.Text = "L-3 Nozzlenose D"
                    Case 34
                        SplatWeaponName.Text = "H-3 Nozzlenose"
                    Case 35
                        SplatWeaponName.Text = "H-3 Nozzlenose D"
                    Case 36
                        SplatWeaponName.Text = "Squeezer"
                    Case 37
                        SplatWeaponName.Text = "Carbon Roller"
                    Case 38
                        SplatWeaponName.Text = "Splat Roller"
                    Case 39
                        SplatWeaponName.Text = "Krak-On Splat Roller"
                    Case 40
                        SplatWeaponName.Text = "Hero Roller Replica"
                    Case 41
                        SplatWeaponName.Text = "Dynamo Roller"
                    Case 42
                        SplatWeaponName.Text = "Gold Dynamo Roller"
                    Case 43
                        SplatWeaponName.Text = "Flingza Roller"
                    Case 44
                        SplatWeaponName.Text = "Foil Flingza Roller"
                    Case 45
                        SplatWeaponName.Text = "Inkbrush"
                    Case 46
                        SplatWeaponName.Text = "Inkbrush Nouveau"
                    Case 47
                        SplatWeaponName.Text = "Octobrush"
                    Case 48
                        SplatWeaponName.Text = "Octobrush Nouveau"
                    Case 49
                        SplatWeaponName.Text = "Herobrush Replica"
                    Case 50
                        SplatWeaponName.Text = "Classic Squiffer"
                    Case 51
                        SplatWeaponName.Text = "Splat Charger"
                    Case 52
                        SplatWeaponName.Text = "Firefin Splat Charger"
                    Case 53
                        SplatWeaponName.Text = "Hero Charger Replica"
                    Case 54
                        SplatWeaponName.Text = "Splatterscope"
                    Case 55
                        SplatWeaponName.Text = "Firefin Splatterscope"
                    Case 56
                        SplatWeaponName.Text = "E-liter 4K"
                    Case 57
                        SplatWeaponName.Text = "Custom E-liter 4K"
                    Case 58
                        SplatWeaponName.Text = "E-liter 4K Scope"
                    Case 59
                        SplatWeaponName.Text = "Custom E-liter 4K Scope"
                    Case 60
                        SplatWeaponName.Text = "Bamboozler 14 Mk I"
                    Case 61
                        SplatWeaponName.Text = "Goo Tuber"
                    Case 62
                        SplatWeaponName.Text = "Custom Goo Tuber"
                    Case 63
                        SplatWeaponName.Text = "Slosher"
                    Case 64
                        SplatWeaponName.Text = "Slosher Deco"
                    Case 65
                        SplatWeaponName.Text = "Hero Slosher Replica"
                    Case 66
                        SplatWeaponName.Text = "Tri-Slosher"
                    Case 67
                        SplatWeaponName.Text = "Sloshing Machine"
                    Case 68
                        SplatWeaponName.Text = "Sloshing Machine Neo"
                    Case 69
                        SplatWeaponName.Text = "Mini Splatling"
                    Case 70
                        SplatWeaponName.Text = "Zink Mini Splatling"
                    Case 71
                        SplatWeaponName.Text = "Heavy Splatling"
                    Case 72
                        SplatWeaponName.Text = "Heavy Splatling Deco"
                    Case 73
                        SplatWeaponName.Text = "Hero Splatling Replica"
                    Case 74
                        SplatWeaponName.Text = "Hydra Splatling"
                    Case 75
                        SplatWeaponName.Text = "Dapple Dualies"
                    Case 76
                        SplatWeaponName.Text = "Dapple Dualies Nouveau"
                    Case 77
                        SplatWeaponName.Text = "Splat Dualies"
                    Case 78
                        SplatWeaponName.Text = "Enperry Splat Dualies"
                    Case 79
                        SplatWeaponName.Text = "Hero Dualie Replicas"
                    Case 80
                        SplatWeaponName.Text = "Glooga Dualies"
                    Case 81
                        SplatWeaponName.Text = "Dualie Squelchers"
                    Case 82
                        SplatWeaponName.Text = "Dark Tetra Dualies"
                    Case 83
                        SplatWeaponName.Text = "Splat Brella"
                    Case 84
                        SplatWeaponName.Text = "Hero Brella Replica"
                    Case 85
                        SplatWeaponName.Text = "Tenta Brella"
                    Case 86
                        SplatWeaponName.Text = "Undercover Brella"
                    Case 87
                        SplatWeaponName.Text = "Sorella Brella"
                    Case 88
                        SplatWeaponName.Text = "Tri-Slosher Nouvea"
                    Case 89
                        SplatWeaponName.Text = "Custom Range Blaster"
                    Case 90
                        SplatWeaponName.Text = "Glooga Dualies Deco"
                    Case 91
                        SplatWeaponName.Text = "New Squiffer"
                    Case 92
                        SplatWeaponName.Text = "Foil Squeezer"
                    Case 93
                        SplatWeaponName.Text = "Neo Splash-o-matic"
                    Case 94
                        SplatWeaponName.Text = "Custom Dualie Squelchers"
                    Case 95
                        SplatWeaponName.Text = "Rapid Blaster Pro Deco"
                    Case 96
                        SplatWeaponName.Text = "Undercover Sorella Brella"
                    Case 97
                        SplatWeaponName.Text = "Carbon Roller Deco"
                    Case 98
                        SplatWeaponName.Text = "Explosher"
                    Case 99
                        SplatWeaponName.Text = "Ballpoint Splatling"
                    Case 100
                        SplatWeaponName.Text = "Octo Shot Replica"
                    Case 101
                        SplatWeaponName.Text = "Bamboozler 14 Mk II"
                    Case 102
                        SplatWeaponName.Text = "Tenta Sorella Brella"
                    Case 103
                        SplatWeaponName.Text = "Nautilus 47"
                    Case 104
                        SplatWeaponName.Text = "Bloblobber"
                    Case 105
                        SplatWeaponName.Text = "Light Tetra Dualies"
                    Case 106
                        SplatWeaponName.Text = "Custom Hydra Splatling"
                    Case 107
                        SplatWeaponName.Text = "Kansa Splattershot"
                    Case 108
                        SplatWeaponName.Text = "Kansa Splat Roller"
                    Case 109
                        SplatWeaponName.Text = "Kansa Charger"
                    Case 110
                        SplatWeaponName.Text = "Kansa Splatterscope"
                    Case 111
                        SplatWeaponName.Text = "Kansa Splat Duelies"
                End Select
            ElseIf checkOnlyShooters.Checked = True Then
                Select Case RandomSplatShooterWeapon
                    Case 1
                        SplatWeaponName.Text = "Sploosh-o-matic"
                    Case 2
                        SplatWeaponName.Text = "Neo Sploosh-o-matic"
                    Case 3
                        SplatWeaponName.Text = "Splattershot Jr."
                    Case 4
                        SplatWeaponName.Text = "Custom Splattershot Jr."
                    Case 5
                        SplatWeaponName.Text = "Splash-o-matic"
                    Case 6
                        SplatWeaponName.Text = "Aerospray MG"
                    Case 7
                        SplatWeaponName.Text = "Aerospray RG"
                    Case 8
                        SplatWeaponName.Text = "Splattershot"
                    Case 9
                        SplatWeaponName.Text = "Tentatek Splattershot"
                    Case 10
                        SplatWeaponName.Text = "Hero Shot Replica"
                    Case 11
                        SplatWeaponName.Text = ".52 Gal"
                    Case 12
                        SplatWeaponName.Text = ".52 Gal Deco"
                    Case 13
                        SplatWeaponName.Text = "N-ZAP '85"
                    Case 14
                        SplatWeaponName.Text = "N-ZAP '89"
                    Case 15
                        SplatWeaponName.Text = "Splattershot Pro"
                    Case 16
                        SplatWeaponName.Text = "Forge Splattershot Pro"
                    Case 17
                        SplatWeaponName.Text = ".96 Gal"
                    Case 18
                        SplatWeaponName.Text = ".96 Gal Deco"
                    Case 19
                        SplatWeaponName.Text = "Jet Squelcher"
                    Case 20
                        SplatWeaponName.Text = "Custom Jet Squelcher"
                    Case 21
                        SplatWeaponName.Text = "L-3 Nozzlenose"
                    Case 22
                        SplatWeaponName.Text = "L-3 Nozzlenose D"
                    Case 23
                        SplatWeaponName.Text = "H-3 Nozzlenose"
                    Case 24
                        SplatWeaponName.Text = "H-3 Nozzlenose D"
                    Case 25
                        SplatWeaponName.Text = "Squeezer"
                    Case 26
                        SplatWeaponName.Text = "Foil Squeezer"
                    Case 27
                        SplatWeaponName.Text = "Neo Splash-o-matic"
                    Case 28
                        SplatWeaponName.Text = "Octo Shot Replica"
                    Case 29
                        SplatWeaponName.Text = "Kansa Splattershot"
                End Select
            ElseIf checkOnlyBlasters.Checked = True Then
                Select Case RandomSplatBlasterWeapon
                    Case 1
                        SplatWeaponName.Text = "Luna Blaster"
                    Case 2
                        SplatWeaponName.Text = "Luna Blaster Neo"
                    Case 3
                        SplatWeaponName.Text = "Blaster"
                    Case 4
                        SplatWeaponName.Text = "Custom Blaster"
                    Case 5
                        SplatWeaponName.Text = "Hero Blaster Replica"
                    Case 6
                        SplatWeaponName.Text = "Range Blaster"
                    Case 7
                        SplatWeaponName.Text = "Clash Blaster"
                    Case 8
                        SplatWeaponName.Text = "Clash Blaster Neo"
                    Case 9
                        SplatWeaponName.Text = "Rapid Blaster"
                    Case 10
                        SplatWeaponName.Text = "Rapid Blaster Deco"
                    Case 11
                        SplatWeaponName.Text = "Rapid Blaster Pro"
                    Case 12
                        SplatWeaponName.Text = "Custom Range Blaster"
                    Case 13
                        SplatWeaponName.Text = "Rapid Blaster Pro Deco"
                End Select
            ElseIf checkOnlyRollers.Checked = True Then
                Select Case RandomSplatRollerWeapon
                    Case 1
                        SplatWeaponName.Text = "Carbon Roller"
                    Case 2
                        SplatWeaponName.Text = "Splat Roller"
                    Case 3
                        SplatWeaponName.Text = "Krak-On Splat Roller"
                    Case 4
                        SplatWeaponName.Text = "Hero Roller Replica"
                    Case 5
                        SplatWeaponName.Text = "Dynamo Roller"
                    Case 6
                        SplatWeaponName.Text = "Gold Dynamo Roller"
                    Case 7
                        SplatWeaponName.Text = "Flingza Roller"
                    Case 8
                        SplatWeaponName.Text = "Foil Flingza Roller"
                    Case 9
                        SplatWeaponName.Text = "Inkbrush"
                    Case 10
                        SplatWeaponName.Text = "Inkbrush Nouveau"
                    Case 11
                        SplatWeaponName.Text = "Octobrush"
                    Case 12
                        SplatWeaponName.Text = "Octobrush Nouveau"
                    Case 13
                        SplatWeaponName.Text = "Herobrush Replica"
                    Case 14
                        SplatWeaponName.Text = "Carbon Roller Deco"
                    Case 15
                        SplatWeaponName.Text = "Kansa Splat Roller"
                End Select
            ElseIf checkOnlyChargers.Checked = True Then
                Select Case RandomSplatChargerWeapon
                    Case 1
                        SplatWeaponName.Text = "Classic Squiffer"
                    Case 2
                        SplatWeaponName.Text = "Splat Charger"
                    Case 3
                        SplatWeaponName.Text = "Firefin Splat Charger"
                    Case 4
                        SplatWeaponName.Text = "Hero Charger Replica"
                    Case 5
                        SplatWeaponName.Text = "Splatterscope"
                    Case 6
                        SplatWeaponName.Text = "Firefin Splatterscope"
                    Case 7
                        SplatWeaponName.Text = "E-liter 4K"
                    Case 8
                        SplatWeaponName.Text = "Custom E-liter 4K"
                    Case 9
                        SplatWeaponName.Text = "E-liter 4K Scope"
                    Case 10
                        SplatWeaponName.Text = "Custom E-liter 4K Scope"
                    Case 11
                        SplatWeaponName.Text = "Bamboozler 14 Mk I"
                    Case 12
                        SplatWeaponName.Text = "Goo Tuber"
                    Case 13
                        SplatWeaponName.Text = "Custom Goo Tuber"
                    Case 14
                        SplatWeaponName.Text = "New Squiffer"
                    Case 15
                        SplatWeaponName.Text = "Bamboozler 14 Mk II"
                    Case 16
                        SplatWeaponName.Text = "Kansa Charger"
                    Case 17
                        SplatWeaponName.Text = "Kansa Splatterscope"
                End Select
            ElseIf checkOnlySloshers.Checked = True Then
                Select Case RandomSplatSlosherWeapon
                    Case 1
                        SplatWeaponName.Text = "Slosher"
                    Case 2
                        SplatWeaponName.Text = "Slosher Deco"
                    Case 3
                        SplatWeaponName.Text = "Hero Slosher Replica"
                    Case 4
                        SplatWeaponName.Text = "Tri-Slosher"
                    Case 5
                        SplatWeaponName.Text = "Sloshing Machine"
                    Case 6
                        SplatWeaponName.Text = "Sloshing Machine Neo"
                    Case 7
                        SplatWeaponName.Text = "Tri-Slosher Nouvea"
                    Case 8
                        SplatWeaponName.Text = "Explosher"
                    Case 9
                        SplatWeaponName.Text = "Bloblobber"
                End Select
            ElseIf checkOnlySplatlings.Checked = True Then
                Select Case RandomSplatSplatlingWeapon
                    Case 1
                        SplatWeaponName.Text = "Mini Splatling"
                    Case 2
                        SplatWeaponName.Text = "Zink Mini Splatling"
                    Case 3
                        SplatWeaponName.Text = "Heavy Splatling"
                    Case 4
                        SplatWeaponName.Text = "Heavy Splatling Deco"
                    Case 5
                        SplatWeaponName.Text = "Hero Splatling Replica"
                    Case 6
                        SplatWeaponName.Text = "Hydra Splatling"
                    Case 7
                        SplatWeaponName.Text = "Ballpoint Splatling"
                    Case 8
                        SplatWeaponName.Text = "Nautilus 47"
                    Case 9
                        SplatWeaponName.Text = "Custom Hydra Splatling"
                End Select
            ElseIf checkOnlyDuelies.Checked = True Then
                Select Case RandomSplatDuelieWeapon
                    Case 1
                        SplatWeaponName.Text = "Dapple Dualies"
                    Case 2
                        SplatWeaponName.Text = "Dapple Dualies Nouveau"
                    Case 3
                        SplatWeaponName.Text = "Splat Dualies"
                    Case 4
                        SplatWeaponName.Text = "Enperry Splat Dualies"
                    Case 5
                        SplatWeaponName.Text = "Hero Dualie Replicas"
                    Case 6
                        SplatWeaponName.Text = "Glooga Dualies"
                    Case 7
                        SplatWeaponName.Text = "Dualie Squelchers"
                    Case 8
                        SplatWeaponName.Text = "Dark Tetra Dualies"
                    Case 9
                        SplatWeaponName.Text = "Glooga Dualies Deco"
                    Case 10
                        SplatWeaponName.Text = "Custom Dualie Squelchers"
                    Case 11
                        SplatWeaponName.Text = "Light Tetra Dualies"
                    Case 12
                        SplatWeaponName.Text = "Kansa Splat Duelies"
                End Select
            ElseIf checkOnlyBrellas.Checked = True Then
                Select Case RandomSplatBrellaWeapon
                    Case 1
                        SplatWeaponName.Text = "Splat Brella"
                    Case 2
                        SplatWeaponName.Text = "Hero Brella Replica"
                    Case 3
                        SplatWeaponName.Text = "Tenta Brella"
                    Case 4
                        SplatWeaponName.Text = "Undercover Brella"
                    Case 5
                        SplatWeaponName.Text = "Sorella Brella"
                    Case 6
                        SplatWeaponName.Text = "Undercover Sorella Brella"
                    Case 7
                        SplatWeaponName.Text = "Tenta Sorella Brella"
                End Select
            End If

            Select Case RandomSplatHeadgear
                Case 1
                    SplatHeadgearName.Text = "White Headband"
                Case 2
                    SplatHeadgearName.Text = "Urchins Cap"
                Case 3
                    SplatHeadgearName.Text = "Lightweight Cap"
                Case 4
                    SplatHeadgearName.Text = "Takoroka Mesh"
                Case 5
                    SplatHeadgearName.Text = "Streetstyle Cap"
                Case 6
                    SplatHeadgearName.Text = "Squidvader Cap"
                Case 7
                    SplatHeadgearName.Text = "Camo Mesh"
                Case 8
                    SplatHeadgearName.Text = "Five-Panel Cap"
                Case 9
                    SplatHeadgearName.Text = "Backwards Cap"
                Case 10
                    SplatHeadgearName.Text = "Two-Stripe Mesh"
                Case 11
                    SplatHeadgearName.Text = "Jet Cap"
                Case 12
                    SplatHeadgearName.Text = "Cycling Cap"
                Case 13
                    SplatHeadgearName.Text = "Cycle King Cap"
                Case 14
                    SplatHeadgearName.Text = "King Flip Mesh"
                Case 15
                    SplatHeadgearName.Text = "Hickory Work Cap"
                Case 16
                    SplatHeadgearName.Text = "Woolly Urchins Classic"
                Case 17
                    SplatHeadgearName.Text = "Jellyvader Cap"
                Case 18
                    SplatHeadgearName.Text = "House-Tag Denim Cap"
                Case 19
                    SplatHeadgearName.Text = "Bobble Hat"
                Case 20
                    SplatHeadgearName.Text = "Short Beanie"
                Case 21
                    SplatHeadgearName.Text = "Striped Beanie"
                Case 22
                    SplatHeadgearName.Text = "Sporty Bobble Hat"
                Case 23
                    SplatHeadgearName.Text = "Special Forces Beret"
                Case 24
                    SplatHeadgearName.Text = "Knitted Hat"
                Case 25
                    SplatHeadgearName.Text = "Annaki Beret"
                Case 26
                    SplatHeadgearName.Text = "Yamagiri Beanie"
                Case 27
                    SplatHeadgearName.Text = "Sneaky Beanie"
                Case 28
                    SplatHeadgearName.Text = "Retro Specs"
                Case 29
                    SplatHeadgearName.Text = "Splash Goggles"
                Case 30
                    SplatHeadgearName.Text = "Pilot Goggles"
                Case 31
                    SplatHeadgearName.Text = "Tinted Shades"
                Case 32
                    SplatHeadgearName.Text = "Black Arrowbands"
                Case 33
                    SplatHeadgearName.Text = "Snorkel Mask"
                Case 34
                    SplatHeadgearName.Text = "Fake Contacts"
                Case 35
                    SplatHeadgearName.Text = "18K Aviators"
                Case 36
                    SplatHeadgearName.Text = "Full Moon Glasses"
                Case 37
                    SplatHeadgearName.Text = "Half-Rim Glasses"
                Case 38
                    SplatHeadgearName.Text = "Double Egg Shades"
                Case 39
                    SplatHeadgearName.Text = "Safari Hat"
                Case 40
                    SplatHeadgearName.Text = "Jungle Hat"
                Case 41
                    SplatHeadgearName.Text = "Camping Hat"
                Case 42
                    SplatHeadgearName.Text = "Blowfish Bell Hat"
                Case 43
                    SplatHeadgearName.Text = "Bamboo Hat"
                Case 44
                    SplatHeadgearName.Text = "Straw Boater"
                Case 45
                    SplatHeadgearName.Text = "Classic Straw Boater"
                Case 46
                    SplatHeadgearName.Text = "Treasure Hunter"
                Case 47
                    SplatHeadgearName.Text = "Bucket Hat"
                Case 48
                    SplatHeadgearName.Text = "Patched Hat"
                Case 49
                    SplatHeadgearName.Text = "Tulip Parasol"
                Case 50
                    SplatHeadgearName.Text = "Fugu Bell Hat"
                Case 51
                    SplatHeadgearName.Text = "Studio Headphones"
                Case 52
                    SplatHeadgearName.Text = "Designer Headphones"
                Case 53
                    SplatHeadgearName.Text = "Noise Cancelers"
                Case 54
                    SplatHeadgearName.Text = "Squidfin Hook Cans"
                Case 55
                    SplatHeadgearName.Text = "Squidlife Headphones"
                Case 56
                    SplatHeadgearName.Text = "Studio Octophones"
                Case 57
                    SplatHeadgearName.Text = "FishFry Visor"
                Case 58
                    SplatHeadgearName.Text = "Sun Visor"
                Case 59
                    SplatHeadgearName.Text = "Takoroka Visor"
                Case 60
                    SplatHeadgearName.Text = "Face Visor"
                Case 61
                    SplatHeadgearName.Text = "Bike Helmet"
                Case 62
                    SplatHeadgearName.Text = "Stealth Goggles"
                Case 63
                    SplatHeadgearName.Text = "Skate Helmet"
                Case 64
                    SplatHeadgearName.Text = "Visor Skate Helmet"
                Case 65
                    SplatHeadgearName.Text = "MTB Helmet"
                Case 66
                    SplatHeadgearName.Text = "Hockey Helmet"
                Case 67
                    SplatHeadgearName.Text = "Matte Bike Helmet"
                Case 68
                    SplatHeadgearName.Text = "Moist Ghillie Helmet"
                Case 69
                    SplatHeadgearName.Text = "Paintball Mask"
                Case 70
                    SplatHeadgearName.Text = "Paisley Bandana"
                Case 71
                    SplatHeadgearName.Text = "Skull Bandana"
                Case 72
                    SplatHeadgearName.Text = "Painter's Mask"
                Case 73
                    SplatHeadgearName.Text = "Annaki Mask"
                Case 74
                    SplatHeadgearName.Text = "Squid Facemask"
                Case 75
                    SplatHeadgearName.Text = "Firefin Facemask"
                Case 76
                    SplatHeadgearName.Text = "King Facemask"
                Case 77
                    SplatHeadgearName.Text = "Motocross Nose Guard"
                Case 78
                    SplatHeadgearName.Text = "Forge Mask"
                Case 79
                    SplatHeadgearName.Text = "B-ball Headband"
                Case 80
                    SplatHeadgearName.Text = "Squash Headband"
                Case 81
                    SplatHeadgearName.Text = "Tennis Headband"
                Case 82
                    SplatHeadgearName.Text = "Soccer Headband"
                Case 83
                    SplatHeadgearName.Text = "FishFry Biscuit Bandana"
                Case 84
                    SplatHeadgearName.Text = "Eminence Cuff"
                Case 85
                    SplatHeadgearName.Text = "Headlamp Helmet"
                Case 86
                    SplatHeadgearName.Text = "Dust Blocker 2000"
                Case 87
                    SplatHeadgearName.Text = "Welding Mask"
                Case 88
                    SplatHeadgearName.Text = "Cap of Legend"
                Case 89
                    SplatHeadgearName.Text = "Oceanic Hard Hat"
                Case 90
                    SplatHeadgearName.Text = "Squid Hairclip"
                Case 91
                    SplatHeadgearName.Text = "Samurai Helmet"
                Case 92
                    SplatHeadgearName.Text = "Power Mask"
                Case 93
                    SplatHeadgearName.Text = "Squid Clip-Ons"
                Case 94
                    SplatHeadgearName.Text = "Squinja Mask"
                Case 95
                    SplatHeadgearName.Text = "Power Mask Mk I"
                Case 96
                    SplatHeadgearName.Text = "Hero Headset Replica"
                Case 97
                    SplatHeadgearName.Text = "Armor Helmet Replica"
                Case 98
                    SplatHeadgearName.Text = "Hero Headphones Replica"
                Case 98
                    SplatHeadgearName.Text = "Annaki Beret and Glasses"
                Case 99
                    SplatHeadgearName.Text = "Do-Rag, Cap, and Glasses"
                Case 100
                    SplatHeadgearName.Text = "Gas Mask"
                Case 101
                    SplatHeadgearName.Text = "Golf Visor"
                Case 102
                    SplatHeadgearName.Text = "Hothouse Hat"
                Case 103
                    SplatHeadgearName.Text = "Jogging Headband"
                Case 104
                    SplatHeadgearName.Text = "Long-Billed Cap"
                Case 105
                    SplatHeadgearName.Text = "Mountie Hat"
                Case 106
                    SplatHeadgearName.Text = "Octoglasses"
                Case 107
                    SplatHeadgearName.Text = "Octo Tackle Helmet Deco"
                Case 108
                    SplatHeadgearName.Text = "Pilot Hat"
                Case 109
                    SplatHeadgearName.Text = "Swim Goggles"
                Case 110
                    SplatHeadgearName.Text = "Toni Kensa Goggles"
                Case 111
                    SplatHeadgearName.Text = "White Arrowbands"
                Case 112
                    SplatHeadgearName.Text = "Zekko Cap"
                Case 113
                    SplatHeadgearName.Text = "Zekko Mesh"
                Case 114
                    SplatHeadgearName.Text = "Octoking Facemask"
                Case 115
                    SplatHeadgearName.Text = "Black FishFry Bandana"
                Case 116
                    SplatHeadgearName.Text = "Blowfish Newsie"
                Case 117
                    SplatHeadgearName.Text = "Deca Tackle Visor Helmet"
                Case 118
                    SplatHeadgearName.Text = "Digi-Camo Forge Mask"
                Case 119
                    SplatHeadgearName.Text = "Ink-Guard Goggles"
                Case 120
                    SplatHeadgearName.Text = "Seashell Bamboo Hat"
                Case 121
                    SplatHeadgearName.Text = "Squid Nordic"
                Case 122
                    SplatHeadgearName.Text = "Squid-Stitch Cap"
                Case 123
                    SplatHeadgearName.Text = "SV925 Circle Shades"
                Case 124
                    SplatHeadgearName.Text = "Octoling Shades"
                Case 125
                    SplatHeadgearName.Text = "Null Visor Replica"
                Case 126
                    SplatHeadgearName.Text = "Old-Timey Hat"
                Case 127
                    SplatHeadgearName.Text = "Conductor Cap"
                Case 128
                    SplatHeadgearName.Text = "Golden Toothpick"
                Case 129
                    SplatHeadgearName.Text = "Pearlescent Crown"
                Case 130
                    SplatHeadgearName.Text = "Marinated Headphones"
                Case 131
                    SplatHeadgearName.Text = "Octoleet Goggles"
            End Select

            Select Case RandomSplatClothing
                Case 1
                    SplatClothingName.Text = "Basic Tee"
                Case 2
                    SplatClothingName.Text = "White Tee"
                Case 3
                    SplatClothingName.Text = "Black Squideye"
                Case 4
                    SplatClothingName.Text = "Sky-Blue Squideye"
                Case 5
                    SplatClothingName.Text = "Rockenberg White"
                Case 6
                    SplatClothingName.Text = "Black Tee"
                Case 7
                    SplatClothingName.Text = "Sunny-Day Tee"
                Case 8
                    SplatClothingName.Text = "Rainy-Day Tee"
                Case 9
                    SplatClothingName.Text = "Reggae Tee"
                Case 10
                    SplatClothingName.Text = "Fugu Tee"
                Case 11
                    SplatClothingName.Text = "Mint Tee"
                Case 12
                    SplatClothingName.Text = "Grape Tee"
                Case 13
                    SplatClothingName.Text = "Red Vector Tee"
                Case 14
                    SplatClothingName.Text = "Blue Peaks Tee"
                Case 15
                    SplatClothingName.Text = "Pirate-Stripe Tee"
                Case 16
                    SplatClothingName.Text = "Sailor-Stripe Tee"
                Case 17
                    SplatClothingName.Text = "White 8-Bit FishFry"
                Case 18
                    SplatClothingName.Text = "Black 8-Bit FishFry"
                Case 19
                    SplatClothingName.Text = "White Anchor Tee"
                Case 20
                    SplatClothingName.Text = "Black Anchor Tee"
                Case 21
                    SplatClothingName.Text = "Carnivore Tee"
                Case 22
                    SplatClothingName.Text = "Pearl Tee"
                Case 23
                    SplatClothingName.Text = "Black V-Neck Tee"
                Case 24
                    SplatClothingName.Text = "White Deca Logo Tee"
                Case 25
                    SplatClothingName.Text = "Half-Sleeve Sweater"
                Case 26
                    SplatClothingName.Text = "King Jersey"
                Case 27
                    SplatClothingName.Text = "Gray 8-Bit FishFry"
                Case 28
                    SplatClothingName.Text = "White V-Neck Tee"
                Case 29
                    SplatClothingName.Text = "White Urchin Rock Tee"
                Case 30
                    SplatClothingName.Text = "Black Urchin Rock Tee"
                Case 31
                    SplatClothingName.Text = "Wet Floor Band Tee"
                Case 32
                    SplatClothingName.Text = "Squid Squad Band Tee"
                Case 33
                    SplatClothingName.Text = "Navy Deca Logo Tee"
                Case 34
                    SplatClothingName.Text = "Mister Shrug Tee"
                Case 35
                    SplatClothingName.Text = "Chirpy Chips Band Tee"
                Case 36
                    SplatClothingName.Text = "Hightide Era Band Tee"
                Case 37
                    SplatClothingName.Text = "Red V-Neck Limited Tee"
                Case 38
                    SplatClothingName.Text = "Green V-Neck Limited Tee"
                Case 39
                    SplatClothingName.Text = "ω-3 Tee"
                Case 40
                    SplatClothingName.Text = "Firewave Tee"
                Case 41
                    SplatClothingName.Text = "Takoroka Galactic Tie Dye"
                Case 42
                    SplatClothingName.Text = "Takoroka Rainbow Tie Dye"
                Case 43
                    SplatClothingName.Text = "Tentatek Slogan Tee"
                Case 44
                    SplatClothingName.Text = "Icewave Tee"
                Case 45
                    SplatClothingName.Text = "White Striped LS"
                Case 46
                    SplatClothingName.Text = "Black LS"
                Case 47
                    SplatClothingName.Text = "Purple Camo LS"
                Case 48
                    SplatClothingName.Text = "Navy Striped LS"
                Case 49
                    SplatClothingName.Text = "Zekko Baseball LS"
                Case 50
                    SplatClothingName.Text = "Varsity Baseball LS"
                Case 51
                    SplatClothingName.Text = "Black Baseball LS"
                Case 52
                    SplatClothingName.Text = "White Baseball LS"
                Case 53
                    SplatClothingName.Text = "Green Striped LS"
                Case 54
                    SplatClothingName.Text = "Squidmark LS"
                Case 55
                    SplatClothingName.Text = "Pink Easy-Stripe Shirt"
                Case 56
                    SplatClothingName.Text = "Inkopolis Squaps Jersey"
                Case 57
                    SplatClothingName.Text = "Annaki Drive Tee"
                Case 58
                    SplatClothingName.Text = "Lime Easy-Stripe Shirt"
                Case 59
                    SplatClothingName.Text = "Annaki Evolution Tee"
                Case 60
                    SplatClothingName.Text = "Zekko Long Carrot Tee"
                Case 61
                    SplatClothingName.Text = "Zekko Long Radish Tee"
                Case 62
                    SplatClothingName.Text = "White Layered LS"
                Case 63
                    SplatClothingName.Text = "Yellow Layered LS"
                Case 64
                    SplatClothingName.Text = "Zink Layered LS"
                Case 65
                    SplatClothingName.Text = "Layered Anchor LS"
                Case 66
                    SplatClothingName.Text = "Choco Layered LS"
                Case 67
                    SplatClothingName.Text = "Part-Time Pirate"
                Case 68
                    SplatClothingName.Text = "Layered Vector LS"
                Case 69
                    SplatClothingName.Text = "Green Tee"
                Case 70
                    SplatClothingName.Text = "Red Tentatek Tee"
                Case 71
                    SplatClothingName.Text = "Blue Tentatek Tee"
                Case 72
                    SplatClothingName.Text = "Octo Layered LS"
                Case 73
                    SplatClothingName.Text = "Shrimp-Pink Polo"
                Case 74
                    SplatClothingName.Text = "Tricolor Rugby"
                Case 75
                    SplatClothingName.Text = "Sage Polo"
                Case 76
                    SplatClothingName.Text = "Black Polo"
                Case 77
                    SplatClothingName.Text = "Cycling Shirt"
                Case 78
                    SplatClothingName.Text = "Cycle King Jersey"
                Case 79
                    SplatClothingName.Text = "Slipstream United"
                Case 80
                    SplatClothingName.Text = "FC Albacore"
                Case 81
                    SplatClothingName.Text = "Olive Ski Jacket"
                Case 82
                    SplatClothingName.Text = "Takoroka Nylon Vintage"
                Case 83
                    SplatClothingName.Text = "Berry Ski Jacket"
                Case 84
                    SplatClothingName.Text = "Varsity Jacket"
                Case 85
                    SplatClothingName.Text = "School Jersey"
                Case 86
                    SplatClothingName.Text = "Black Inky Rider"
                Case 87
                    SplatClothingName.Text = "White Inky Rider"
                Case 88
                    SplatClothingName.Text = "Orange Cardigan"
                Case 89
                    SplatClothingName.Text = "Forge Inkling Parka"
                Case 90
                    SplatClothingName.Text = "Blue Sailor Suit"
                Case 91
                    SplatClothingName.Text = "White Sailor Suit"
                Case 92
                    SplatClothingName.Text = "Squid Satin Jacket"
                Case 93
                    SplatClothingName.Text = "Zapfish Satin Jacket"
                Case 94
                    SplatClothingName.Text = "Krak-On 528"
                Case 95
                    SplatClothingName.Text = "Chilly Mountain Coat"
                Case 96
                    SplatClothingName.Text = "Takoroka Windcrusher"
                Case 97
                    SplatClothingName.Text = "Matcha Down Jacket"
                Case 98
                    SplatClothingName.Text = "FA-01 Jacket"
                Case 99
                    SplatClothingName.Text = "FA-01 Reversed"
                Case 100
                    SplatClothingName.Text = "Pullover Coat"
                Case 101
                    SplatClothingName.Text = "Kensa Coat"
                Case 102
                    SplatClothingName.Text = "Birded Corduroy Jacket"
                Case 103
                    SplatClothingName.Text = "Zekko Redleaf Coat"
                Case 104
                    SplatClothingName.Text = "Eggplant Mountain Coat"
                Case 105
                    SplatClothingName.Text = "Zekko Jade Coat"
                Case 106
                    SplatClothingName.Text = "Light Bomber Jacket"
                Case 107
                    SplatClothingName.Text = "Brown FA-11 Bomber"
                Case 108
                    SplatClothingName.Text = "Gray FA-11 Bomber"
                Case 109
                    SplatClothingName.Text = "Milky Eminence Jacket"
                Case 110
                    SplatClothingName.Text = "Navy Eminence Jacket"
                Case 111
                    SplatClothingName.Text = "Tumeric Zekko Coat"
                Case 112
                    SplatClothingName.Text = "Custom Painted F-3"
                Case 113
                    SplatClothingName.Text = "Dark Bomber Jacket"
                Case 114
                    SplatClothingName.Text = "Moist Ghillie Suit"
                Case 115
                    SplatClothingName.Text = "B-ball Jersey (Home)"
                Case 116
                    SplatClothingName.Text = "B-ball Jersey (Away)"
                Case 117
                    SplatClothingName.Text = "White King Tank"
                Case 118
                    SplatClothingName.Text = "Slash King Tank"
                Case 119
                    SplatClothingName.Text = "Navy King Tank"
                Case 120
                    SplatClothingName.Text = "Gray College Sweat"
                Case 121
                    SplatClothingName.Text = "Squidmark Sweat"
                Case 122
                    SplatClothingName.Text = "Retro Sweat"
                Case 123
                    SplatClothingName.Text = "Firefin Navy Sweat"
                Case 124
                    SplatClothingName.Text = "Reel Sweat"
                Case 125
                    SplatClothingName.Text = "Anchor Sweat"
                Case 126
                    SplatClothingName.Text = "Negative Longcuff Sweater"
                Case 127
                    SplatClothingName.Text = "Short Knit Layers"
                Case 128
                    SplatClothingName.Text = "Positive Longcuff Sweater"
                Case 129
                    SplatClothingName.Text = "Annaki Blue Cuff"
                Case 130
                    SplatClothingName.Text = "Annaki Red Cuff"
                Case 131
                    SplatClothingName.Text = "N-Pacer Sweat"
                Case 132
                    SplatClothingName.Text = "Lumberjack Shirt"
                Case 133
                    SplatClothingName.Text = "Rodeo Shirt"
                Case 134
                    SplatClothingName.Text = "Green-Check Shirt"
                Case 135
                    SplatClothingName.Text = "White Shirt"
                Case 136
                    SplatClothingName.Text = "Urchins Jersey"
                Case 137
                    SplatClothingName.Text = "Aloha Shirt"
                Case 138
                    SplatClothingName.Text = "Baby-Jelly Shirt"
                Case 139
                    SplatClothingName.Text = "Baseball Jersey"
                Case 140
                    SplatClothingName.Text = "Gray Mixed Shirt"
                Case 141
                    SplatClothingName.Text = "Vintage Check Shirt"
                Case 142
                    SplatClothingName.Text = "Round-Collar Shirt"
                Case 143
                    SplatClothingName.Text = "Logo Aloha Shirt"
                Case 144
                    SplatClothingName.Text = "Striped Shirt"
                Case 145
                    SplatClothingName.Text = "Linen Shirt"
                Case 146
                    SplatClothingName.Text = "Shirt and Tie"
                Case 147
                    SplatClothingName.Text = "Hula Punk Shirt"
                Case 148
                    SplatClothingName.Text = "Octobowler Shirt"
                Case 149
                    SplatClothingName.Text = "Inkfall Shirt"
                Case 150
                    SplatClothingName.Text = "Crimson Parashooter"
                Case 151
                    SplatClothingName.Text = "Baby-Jelly Shirt and Tie"
                Case 152
                    SplatClothingName.Text = "Prune Parashooter"
                Case 153
                    SplatClothingName.Text = "Red Hula Punk with Tie"
                Case 154
                    SplatClothingName.Text = "Mountain Vest"
                Case 155
                    SplatClothingName.Text = "Dark Urban Vest"
                Case 156
                    SplatClothingName.Text = "Yellow Urban Vest"
                Case 157
                    SplatClothingName.Text = "Squidstar Waistcoat"
                Case 158
                    SplatClothingName.Text = "Fishing Vest"
                Case 159
                    SplatClothingName.Text = "Front Zip Vest"
                Case 160
                    SplatClothingName.Text = "Camo Zip Hoodie"
                Case 161
                    SplatClothingName.Text = "Green Zip Hoodie"
                Case 162
                    SplatClothingName.Text = "Zekko Hoodie"
                Case 163
                    SplatClothingName.Text = "Shirt with Blue Hoodie"
                Case 164
                    SplatClothingName.Text = "Grape Hoodie"
                Case 165
                    SplatClothingName.Text = "Gray Hoodie"
                Case 166
                    SplatClothingName.Text = "Pink Hoodie"
                Case 167
                    SplatClothingName.Text = "Olive Zekko Parka"
                Case 168
                    SplatClothingName.Text = "Squiddor Polo"
                Case 169
                    SplatClothingName.Text = "Anchor Life Vest"
                Case 170
                    SplatClothingName.Text = "Juice Parka"
                Case 171
                    SplatClothingName.Text = "Garden Gear"
                Case 172
                    SplatClothingName.Text = "Crustwear XXL"
                Case 173
                    SplatClothingName.Text = "School Uniform"
                Case 174
                    SplatClothingName.Text = "Samurai Jacket"
                Case 175
                    SplatClothingName.Text = "Power Armor"
                Case 176
                    SplatClothingName.Text = "School Cardigan"
                Case 177
                    SplatClothingName.Text = "Squinja Suit"
                Case 178
                    SplatClothingName.Text = "Power Armor Mk I"
                Case 179
                    SplatClothingName.Text = "Hero Jacket Replica"
                Case 180
                    SplatClothingName.Text = "Armor Jacket Replica"
                Case 181
                    SplatClothingName.Text = "Hero Hoodie Replica"
                Case 182
                    SplatClothingName.Text = "Annaki Flannel Hoodie"
                Case 183
                    SplatClothingName.Text = "Annaki Polpo-Pic Tee"
                Case 184
                    SplatClothingName.Text = "Black Cuttlegear LS"
                Case 185
                    SplatClothingName.Text = "Chili Octo Aloha"
                Case 186
                    SplatClothingName.Text = "Chili-Pepper Ski Jacket"
                Case 187
                    SplatClothingName.Text = "Deep-Octo Satin Jacket"
                Case 188
                    SplatClothingName.Text = "Dots-On-Dots Shirt"
                Case 189
                    SplatClothingName.Text = "Green Velour Octoking Tee"
                Case 190
                    SplatClothingName.Text = "Herbivore Tee"
                Case 191
                    SplatClothingName.Text = "Hothouse Hoodie"
                Case 192
                    SplatClothingName.Text = "Ivory Peaks Tee"
                Case 193
                    SplatClothingName.Text = "Kung-Fu Zip-Up"
                Case 194
                    SplatClothingName.Text = "Missus Shrug Tee"
                Case 195
                    SplatClothingName.Text = "Octarian Retro"
                Case 196
                    SplatClothingName.Text = "Octoking HK Jersey"
                Case 197
                    SplatClothingName.Text = "Octo Tee"
                Case 198
                    SplatClothingName.Text = "Panda Kung-Fu Zip-Up"
                Case 199
                    SplatClothingName.Text = "Red Cuttlegear LS"
                Case 200
                    SplatClothingName.Text = "Silver Tentatek Vest"
                Case 201
                    SplatClothingName.Text = "Squid Yellow Layered LS"
                Case 202
                    SplatClothingName.Text = "Takoroka Crazy Baseball LS"
                Case 203
                    SplatClothingName.Text = "Takoroka Jersey"
                Case 204
                    SplatClothingName.Text = "Toni K. Baseball Jersey"
                Case 205
                    SplatClothingName.Text = "White Leather F-3"
                Case 206
                    SplatClothingName.Text = "Zink LS"
                Case 207
                    SplatClothingName.Text = "Annaki Yellow Cuff"
                Case 208
                    SplatClothingName.Text = "Black Cuttlegear LS"
                Case 209
                    SplatClothingName.Text = "Black Hoodie"
                Case 210
                    SplatClothingName.Text = "Black Layered LS"
                Case 211
                    SplatClothingName.Text = "Black Velour Octoking Tee"
                Case 212
                    SplatClothingName.Text = "Blue 16-Bit FishFry"
                Case 213
                    SplatClothingName.Text = "Camo Layered LS"
                Case 214
                    SplatClothingName.Text = "Dakro Golden Tee"
                Case 215
                    SplatClothingName.Text = "Dakro Nana Tee"
                Case 216
                    SplatClothingName.Text = "Deep-Octo Satin Jacket"
                Case 217
                    SplatClothingName.Text = "Forest Vest"
                Case 218
                    SplatClothingName.Text = "Forge Octarian Jacket"
                Case 219
                    SplatClothingName.Text = "Friend Tee"
                Case 220
                    SplatClothingName.Text = "Gray Vector Tee"
                Case 221
                    SplatClothingName.Text = "Green Cardigan"
                Case 222
                    SplatClothingName.Text = "Ink-Wash Shirt"
                Case 223
                    SplatClothingName.Text = "Khaki 16-Bit FishFry"
                Case 224
                    SplatClothingName.Text = "League Tee"
                Case 225
                    SplatClothingName.Text = "Lob-Stars Jersey"
                Case 226
                    SplatClothingName.Text = "Navy College Sweat"
                Case 227
                    SplatClothingName.Text = "Red-Check Shirt"
                Case 228
                    SplatClothingName.Text = "Retro Gamer Jersey"
                Case 229
                    SplatClothingName.Text = "Rockenberg Black"
                Case 230
                    SplatClothingName.Text = "Rockin' Leather Jacket"
                Case 231
                    SplatClothingName.Text = "Squid-Pattern Waistcoat"
                Case 232
                    SplatClothingName.Text = "Squid-Stitch Tee"
                Case 233
                    SplatClothingName.Text = "Striped Peaks LS"
                Case 234
                    SplatClothingName.Text = "Striped Rugby"
                Case 235
                    SplatClothingName.Text = "Whale-Knit Sweater"
                Case 236
                    SplatClothingName.Text = "White LS"
                Case 237
                    SplatClothingName.Text = "Fresh Octo Tee"
                Case 238
                    SplatClothingName.Text = "Neo Octoling Armor"
                Case 239
                    SplatClothingName.Text = "Null Armor Replica"
                Case 240
                    SplatClothingName.Text = "Old-Timey Clothes"
                Case 241
                    SplatClothingName.Text = "SWC Logo Tee"
                Case 242
                    SplatClothingName.Text = "Pearlescent Hoodie"
                Case 243
                    SplatClothingName.Text = "Marinated Top"
                Case 244
                    SplatClothingName.Text = "Online Jersey"
            End Select

            Select Case RandomSplatShoes
                Case 1
                    SplatShoesName.Text = "Cream Basics"
                Case 2
                    SplatShoesName.Text = "Blue Lo-Tops"
                Case 3
                    SplatShoesName.Text = "White Seahorses"
                Case 4
                    SplatShoesName.Text = "Black Seahorses"
                Case 5
                    SplatShoesName.Text = "Clownfish Basics"
                Case 6
                    SplatShoesName.Text = "Strapping Whites"
                Case 7
                    SplatShoesName.Text = "Strapping Reds"
                Case 8
                    SplatShoesName.Text = "Soccer Shoes"
                Case 9
                    SplatShoesName.Text = "LE Soccer Shoes"
                Case 10
                    SplatShoesName.Text = "Sunny Climbing Shoes"
                Case 11
                    SplatShoesName.Text = "Birch Climbing Shoes"
                Case 12
                    SplatShoesName.Text = "Green Laceups"
                Case 13
                    SplatShoesName.Text = "White Laceless Dakroniks"
                Case 14
                    SplatShoesName.Text = "Blue Laceless Dakroniks"
                Case 15
                    SplatShoesName.Text = "Red Hi-Horses"
                Case 16
                    SplatShoesName.Text = "Zombie Hi-Horses"
                Case 17
                    SplatShoesName.Text = "Purple Hi-Horses"
                Case 18
                    SplatShoesName.Text = "Hunter Hi-Tops"
                Case 19
                    SplatShoesName.Text = "Red Hi-Tops"
                Case 20
                    SplatShoesName.Text = "Gold Hi-Horses"
                Case 21
                    SplatShoesName.Text = "Shark Moccasins"
                Case 22
                    SplatShoesName.Text = "Mawcasins"
                Case 23
                    SplatShoesName.Text = "Mint Dakroniks"
                Case 24
                    SplatShoesName.Text = "Black Dakroniks"
                Case 25
                    SplatShoesName.Text = "Piranha Moccasins"
                Case 26
                    SplatShoesName.Text = "White Norimaki 750s"
                Case 27
                    SplatShoesName.Text = "Black Norimaki 750s"
                Case 28
                    SplatShoesName.Text = "Sunset Orca Hi-Tops"
                Case 29
                    SplatShoesName.Text = "Red and Black Squidkid IV"
                Case 30
                    SplatShoesName.Text = "Blue and Black Squidkid IV"
                Case 31
                    SplatShoesName.Text = "Gray Sea-Slug Hi-Tops"
                Case 32
                    SplatShoesName.Text = "Orca Hi-Tops"
                Case 33
                    SplatShoesName.Text = "Milky Enperrials"
                Case 34
                    SplatShoesName.Text = "Navy Enperrials"
                Case 35
                    SplatShoesName.Text = "Amber Sea Slug Hi-Tops"
                Case 36
                    SplatShoesName.Text = "Yellow Iromaki 750s"
                Case 37
                    SplatShoesName.Text = "Sun and Shade Squidkid IV"
                Case 38
                    SplatShoesName.Text = "Orca Woven Hi-Tops"
                Case 39
                    SplatShoesName.Text = "Green Iromaki 750s"
                Case 40
                    SplatShoesName.Text = "Purple Iromaki 750s"
                Case 41
                    SplatShoesName.Text = "Red Iromaki 750s"
                Case 42
                    SplatShoesName.Text = "Sesame Salt 270s"
                Case 43
                    SplatShoesName.Text = "Pink Trainers"
                Case 44
                    SplatShoesName.Text = "Orange Arrows"
                Case 45
                    SplatShoesName.Text = "Neon Sea Slugs"
                Case 46
                    SplatShoesName.Text = "White Arrows"
                Case 47
                    SplatShoesName.Text = "Cyan Trainers"
                Case 48
                    SplatShoesName.Text = "Red Sea Slugs"
                Case 49
                    SplatShoesName.Text = "Purple Sea Slugs"
                Case 50
                    SplatShoesName.Text = "Crazy Arrows"
                Case 51
                    SplatShoesName.Text = "Black Trainers"
                Case 52
                    SplatShoesName.Text = "Violet Trainers"
                Case 53
                    SplatShoesName.Text = "Canary Trainers"
                Case 54
                    SplatShoesName.Text = "Yellow-Mesh Sneakers"
                Case 55
                    SplatShoesName.Text = "Arrow Pull-Ons"
                Case 56
                    SplatShoesName.Text = "Red-Mesh Sneakers"
                Case 57
                    SplatShoesName.Text = "N-Pacer Ag"
                Case 58
                    SplatShoesName.Text = "N-Pacer Au"
                Case 59
                    SplatShoesName.Text = "Sea Slug Volt 95s"
                Case 60
                    SplatShoesName.Text = "Oyster Clogs"
                Case 61
                    SplatShoesName.Text = "Choco Clogs"
                Case 62
                    SplatShoesName.Text = "Blueberry Casuals"
                Case 63
                    SplatShoesName.Text = "Plum Casuals"
                Case 64
                    SplatShoesName.Text = "Neon Delta Straps"
                Case 65
                    SplatShoesName.Text = "Black Flip-Flops"
                Case 66
                    SplatShoesName.Text = "Snow Delta Straps"
                Case 67
                    SplatShoesName.Text = "Luminous Delta Straps"
                Case 68
                    SplatShoesName.Text = "Red FishFry Sandals"
                Case 69
                    SplatShoesName.Text = "Trail Boots"
                Case 70
                    SplatShoesName.Text = "Pro Trail Boots"
                Case 71
                    SplatShoesName.Text = "Moto Boots"
                Case 72
                    SplatShoesName.Text = "Tan Work Boots"
                Case 73
                    SplatShoesName.Text = "Blue Moto Boots"
                Case 74
                    SplatShoesName.Text = "Green Rain Boots"
                Case 75
                    SplatShoesName.Text = "Acerola Rain Boots"
                Case 76
                    SplatShoesName.Text = "Punk Whites"
                Case 77
                    SplatShoesName.Text = "Punk Cherries"
                Case 78
                    SplatShoesName.Text = "Bubble Rain Boots"
                Case 79
                    SplatShoesName.Text = "Snowy Down Boots"
                Case 80
                    SplatShoesName.Text = "Hunting Boots"
                Case 81
                    SplatShoesName.Text = "Punk Blacks"
                Case 82
                    SplatShoesName.Text = "Deepsea Leather Boots"
                Case 83
                    SplatShoesName.Text = "Moist Ghillie Boots"
                Case 84
                    SplatShoesName.Text = "Blue Slip-Ons"
                Case 85
                    SplatShoesName.Text = "Red Slip-Ons"
                Case 86
                    SplatShoesName.Text = "Squid-Stitch Slip-Ons"
                Case 87
                    SplatShoesName.Text = "Polka-dot Slip-Ons"
                Case 88
                    SplatShoesName.Text = "White Kicks"
                Case 89
                    SplatShoesName.Text = "Cherry Kicks"
                Case 90
                    SplatShoesName.Text = "Turquoise Kicks"
                Case 91
                    SplatShoesName.Text = "Squink Wingtips"
                Case 92
                    SplatShoesName.Text = "Roasted Brogues"
                Case 93
                    SplatShoesName.Text = "Kid Clams"
                Case 94
                    SplatShoesName.Text = "Smoky Wingtips"
                Case 95
                    SplatShoesName.Text = "Navy Red-Soled Wingtips"
                Case 96
                    SplatShoesName.Text = "Gray Yellow-Soled Wingtips"
                Case 97
                    SplatShoesName.Text = "Annaki Habaneros"
                Case 98
                    SplatShoesName.Text = "Angry Rain Boots"
                Case 99
                    SplatShoesName.Text = "Non-slip Senseis"
                Case 100
                    SplatShoesName.Text = "School Shoes"
                Case 101
                    SplatShoesName.Text = "Samurai Shoes"
                Case 102
                    SplatShoesName.Text = "Power Boots"
                Case 103
                    SplatShoesName.Text = "Fringed Loafers"
                Case 104
                    SplatShoesName.Text = "Squinja Boots"
                Case 105
                    SplatShoesName.Text = "Power Boots Mk I"
                Case 106
                    SplatShoesName.Text = "Hero Runner Replicas"
                Case 107
                    SplatShoesName.Text = "Armor Boot Replicas"
                Case 108
                    SplatShoesName.Text = "Hero Snowboots Replicas"
                Case 109
                    SplatShoesName.Text = "Annaki Arachno Boots"
                Case 110
                    SplatShoesName.Text = "Annaki Tigers"
                Case 111
                    SplatShoesName.Text = "Banana Basics"
                Case 112
                    SplatShoesName.Text = "Blue Power Stripes"
                Case 113
                    SplatShoesName.Text = "Blue Sea Slugs"
                Case 114
                    SplatShoesName.Text = "Custom Trail Boots"
                Case 115
                    SplatShoesName.Text = "Icy Down Boots"
                Case 116
                    SplatShoesName.Text = "Musselforge Flip-Flops"
                Case 117
                    SplatShoesName.Text = "New-Leaf Leather Boots"
                Case 118
                    SplatShoesName.Text = "Orange Iromaki 750s"
                Case 119
                    SplatShoesName.Text = "Orca Passion Hi-Tops	"
                Case 120
                    SplatShoesName.Text = "Punk Yellows"
                Case 121
                    SplatShoesName.Text = "Red Power Stripes"
                Case 122
                    SplatShoesName.Text = "Red and White Squidkid V"
                Case 123
                    SplatShoesName.Text = "Red Work Boots"
                Case 124
                    SplatShoesName.Text = "Yellow FishFry Sandals"
                Case 125
                    SplatShoesName.Text = "Toni Kensa Soccer Shoes"
                Case 126
                    SplatShoesName.Text = "Truffle Canvas Hi-Tops"
                Case 127
                    SplatShoesName.Text = "Yellow Seahorses"
                Case 128
                    SplatShoesName.Text = "Annaki Arachno Boots"
                Case 129
                    SplatShoesName.Text = "Athletic Arrows"
                Case 130
                    SplatShoesName.Text = "Black and Blue Squidkid V"
                Case 131
                    SplatShoesName.Text = "Blue Iromaki 750s"
                Case 132
                    SplatShoesName.Text = "Chocolate Dakroniks"
                Case 133
                    SplatShoesName.Text = "Cream Hi-Tops"
                Case 134
                    SplatShoesName.Text = "Honey and Orange Squidkid V"
                Case 135
                    SplatShoesName.Text = "Inky Kid Clams"
                Case 136
                    SplatShoesName.Text = "LE Lo-Tops"
                Case 137
                    SplatShoesName.Text = "N-Pacer CaO"
                Case 138
                    SplatShoesName.Text = "Orange Lo-Tops"
                Case 139
                    SplatShoesName.Text = "Suede Gray Lace-Ups"
                Case 140
                    SplatShoesName.Text = "Suede Marine Lace-Ups"
                Case 141
                    SplatShoesName.Text = "Suede Nation Lace-Ups"
                Case 142
                    SplatShoesName.Text = "Tea-Green Hunting Boots"
                Case 143
                    SplatShoesName.Text = "Toni Kensa Black Hi-Tops"
                Case 144
                    SplatShoesName.Text = "Neo Octoling Boots"
                Case 145
                    SplatShoesName.Text = "Null Boots Replica"
                Case 146
                    SplatShoesName.Text = "Old-Timey Shoes"
                Case 147
                    SplatShoesName.Text = "Pearlescent Kicks"
                Case 148
                    SplatShoesName.Text = "Marinated Slip-Ons"
                Case 149
                    SplatShoesName.Text = "Online Squidkid V Shoes"
            End Select

            If checkOnlyWeaponClothes.Checked = True Then
                SplatStageLabel.Visible = False
                SplatStageName.Visible = False
                SplatModeLabel.Visible = False
                SplatModeName.Visible = False
            Else
                SplatStageLabel.Visible = True
                SplatStageName.Visible = True
                SplatModeLabel.Visible = True
                SplatModeName.Visible = True
                SplatStageLabel.Text = "Stage:"
                SplatModeLabel.Text = "Mode:"
                Select Case RandomSplatStage
                    Case 1
                        SplatStageName.Text = "Blackbelly Skatepark"
                    Case 2
                        SplatStageName.Text = "The Reef"
                    Case 3
                        SplatStageName.Text = "Musselforge Fitness"
                    Case 4
                        SplatStageName.Text = "Starfish Mainstage"
                    Case 5
                        SplatStageName.Text = "Humpback Pump Track"
                    Case 6
                        SplatStageName.Text = "Inkblot Art Academy"
                    Case 7
                        SplatStageName.Text = "Moray Towers"
                    Case 8
                        SplatStageName.Text = "Port Mackerel"
                    Case 9
                        SplatStageName.Text = "Sturgeon Shipyard"
                    Case 10
                        SplatStageName.Text = "Manta Maria"
                    Case 11
                        SplatStageName.Text = "Kelp Dome"
                    Case 12
                        SplatStageName.Text = "Snapper Canal"
                    Case 13
                        SplatStageName.Text = "MakoMart"
                    Case 14
                        SplatStageName.Text = "Walleye Warehouse"
                    Case 15
                        SplatStageName.Text = "Shellendorf Institute"
                    Case 16
                        SplatStageName.Text = "Arowana Mall"
                    Case 17
                        SplatStageName.Text = "Goby Arena"
                    Case 18
                        SplatStageName.Text = "Camp Triggerfish"
                    Case 19
                        SplatStageName.Text = "Wahoo World"
                    Case 20
                        SplatStageName.Text = "New Albacore Hotel"
                    Case 21
                        SplatStageName.Text = "Ancho-V Games"
                End Select

                Select Case RandomSplatMode
                    Case 1
                        SplatModeName.Text = "Turf War"
                    Case 2
                        SplatModeName.Text = "Splat Zones"
                    Case 3
                        SplatModeName.Text = "Tower Control"
                    Case 4
                        SplatModeName.Text = "Rainmaker"
                    Case 5
                        SplatModeName.Text = "Clam Blitz"
                End Select
            End If
        End If

        btnSplatTweet.Enabled = True
    End Sub

    Private Sub checkAllWeapons_CheckedChanged(sender As Object, e As EventArgs) Handles checkAllWeapons.CheckedChanged
        If checkAllWeapons.Checked = True Then
            checkOnlyShooters.Checked = False
            checkOnlyBlasters.Checked = False
            checkOnlyRollers.Checked = False
            checkOnlyChargers.Checked = False
            checkOnlySloshers.Checked = False
            checkOnlySplatlings.Checked = False
            checkOnlyDuelies.Checked = False
            checkOnlyBrellas.Checked = False
        End If
    End Sub

    Private Sub checkOnlyShooters_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlyShooters.CheckedChanged
        If checkOnlyShooters.Checked = True Then
            checkAllWeapons.Checked = False
            checkOnlyBlasters.Checked = False
            checkOnlyRollers.Checked = False
            checkOnlyChargers.Checked = False
            checkOnlySloshers.Checked = False
            checkOnlySplatlings.Checked = False
            checkOnlyDuelies.Checked = False
            checkOnlyBrellas.Checked = False
        End If
    End Sub

    Private Sub checkOnlyBlasters_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlyBlasters.CheckedChanged
        If checkOnlyBlasters.Checked = True Then
            checkAllWeapons.Checked = False
            checkOnlyShooters.Checked = False
            checkOnlyRollers.Checked = False
            checkOnlyChargers.Checked = False
            checkOnlySloshers.Checked = False
            checkOnlySplatlings.Checked = False
            checkOnlyDuelies.Checked = False
            checkOnlyBrellas.Checked = False
        End If
    End Sub

    Private Sub checkOnlyRollers_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlyRollers.CheckedChanged
        If checkOnlyRollers.Checked = True Then
            checkAllWeapons.Checked = False
            checkOnlyShooters.Checked = False
            checkOnlyBlasters.Checked = False
            checkOnlyChargers.Checked = False
            checkOnlySloshers.Checked = False
            checkOnlySplatlings.Checked = False
            checkOnlyDuelies.Checked = False
            checkOnlyBrellas.Checked = False
        End If
    End Sub

    Private Sub checkOnlyChargers_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlyChargers.CheckedChanged
        If checkOnlyChargers.Checked = True Then
            checkAllWeapons.Checked = False
            checkOnlyShooters.Checked = False
            checkOnlyBlasters.Checked = False
            checkOnlyRollers.Checked = False
            checkOnlySloshers.Checked = False
            checkOnlySplatlings.Checked = False
            checkOnlyDuelies.Checked = False
            checkOnlyBrellas.Checked = False
        End If
    End Sub

    Private Sub checkOnlySplatlings_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlySplatlings.CheckedChanged
        If checkOnlySplatlings.Checked = True Then
            checkAllWeapons.Checked = False
            checkOnlyShooters.Checked = False
            checkOnlyBlasters.Checked = False
            checkOnlyRollers.Checked = False
            checkOnlyChargers.Checked = False
            checkOnlySloshers.Checked = False
            checkOnlyDuelies.Checked = False
            checkOnlyBrellas.Checked = False
        End If
    End Sub

    Private Sub checkOnlyDuelies_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlyDuelies.CheckedChanged
        If checkOnlyDuelies.Checked = True Then
            checkAllWeapons.Checked = False
            checkOnlyShooters.Checked = False
            checkOnlyBlasters.Checked = False
            checkOnlyRollers.Checked = False
            checkOnlyChargers.Checked = False
            checkOnlySloshers.Checked = False
            checkOnlySplatlings.Checked = False
            checkOnlyBrellas.Checked = False
        End If
    End Sub

    Private Sub checkOnlyBrellas_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlyBrellas.CheckedChanged
        If checkOnlyBrellas.Checked = True Then
            checkAllWeapons.Checked = False
            checkOnlyShooters.Checked = False
            checkOnlyBlasters.Checked = False
            checkOnlyRollers.Checked = False
            checkOnlyChargers.Checked = False
            checkOnlySloshers.Checked = False
            checkOnlySplatlings.Checked = False
            checkOnlyDuelies.Checked = False
        End If
    End Sub

    Private Sub checkOnlySloshers_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlySloshers.CheckedChanged
        If checkOnlySloshers.Checked = True Then
            checkAllWeapons.Checked = False
            checkOnlyShooters.Checked = False
            checkOnlyBlasters.Checked = False
            checkOnlyRollers.Checked = False
            checkOnlyChargers.Checked = False
            checkOnlySplatlings.Checked = False
            checkOnlyDuelies.Checked = False
            checkOnlyBrellas.Checked = False
        End If
    End Sub

    'Tweet Results button'

    Private Sub btnMK8Tweet_Click(sender As Object, e As EventArgs) Handles btnMK8Tweet.Click
        If checkBattleMode.Checked = True Then
            Process.Start("https://twitter.com/intent/tweet?url=https%3A%2F%2Fgithub.com%2FCodyMKW%2FNintendo-Game-Randomizer%2Freleases&text=" + "%0a" + MK8CharacterLabel.Text + "%20" + MK8CharacterName.Text + "%0a" + MK8VehicleLabel.Text + "%20" + MK8VehicleName.Text + "%0a" + MK8TiresLabel.Text + "%20" + MK8TiresName.Text + "%0a" + MK8GliderLabel.Text + "%20" + MK8GliderName.Text + "%0a" + MK8TrackLabel.Text + "%20" + MK8TrackName.Text + "%0a" + MK8BattleModeLabel.Text + "%20" + MK8BattleModeName.Text + "%0aGenerated%20with&hashtags=MarioKart8Deluxe%2CNintendoSwitch%2CNintendoGameRandomizer")
        ElseIf checkOnlyCharAndParts.Checked = True Then
            Process.Start("https://twitter.com/intent/tweet?url=https%3A%2F%2Fgithub.com%2FCodyMKW%2FNintendo-Game-Randomizer%2Freleases&text=" + "%0a" + MK8CharacterLabel.Text + "%20" + MK8CharacterName.Text + "%0a" + MK8VehicleLabel.Text + "%20" + MK8VehicleName.Text + "%0a" + MK8TiresLabel.Text + "%20" + MK8TiresName.Text + "%0a" + MK8GliderLabel.Text + "%20" + MK8GliderName.Text + "%0aGenerated%20with&hashtags=MarioKart8Deluxe%2CNintendoSwitch%2CNintendoGameRandomizer")
        Else
            Process.Start("https://twitter.com/intent/tweet?url=https%3A%2F%2Fgithub.com%2FCodyMKW%2FNintendo-Game-Randomizer%2Freleases&text=" + "%0a" + MK8CharacterLabel.Text + "%20" + MK8CharacterName.Text + "%0a" + MK8VehicleLabel.Text + "%20" + MK8VehicleName.Text + "%0a" + MK8TiresLabel.Text + "%20" + MK8TiresName.Text + "%0a" + MK8GliderLabel.Text + "%20" + MK8GliderName.Text + "%0a" + MK8TrackLabel.Text + "%20" + MK8TrackName.Text + "%0aGenerated%20with&hashtags=MarioKart8Deluxe%2CNintendoSwitch%2CNintendoGameRandomizer")
        End If
    End Sub

    Private Sub checkBattleMode_CheckedChanged(sender As Object, e As EventArgs) Handles checkBattleMode.CheckedChanged
        btnMK8Tweet.Enabled = False
    End Sub

    Private Sub btnSplatTweet_Click(sender As Object, e As EventArgs) Handles btnSplatTweet.Click
        If checkOnlyWeaponClothes.Checked = True Then
            Process.Start("https://twitter.com/intent/tweet?url=https%3A%2F%2Fgithub.com%2FCodyMKW%2FNintendo-Game-Randomizer%2Freleases&text=" + "%0a" + SplatWeaponLabel.Text + "%20" + SplatWeaponName.Text + "%0a" + SplatHeadgearLabel.Text + "%20" + SplatHeadgearName.Text + "%0a" + SplatClothingLabel.Text + "%20" + SplatClothingName.Text + "%0a" + SplatShoesLabel.Text + "%20" + SplatShoesName.Text + "%0aGenerated%20with&hashtags=Splatoon2%2CNintendoSwitch%2CNintendoGameRandomizer")
        Else
            Process.Start("https://twitter.com/intent/tweet?url=https%3A%2F%2Fgithub.com%2FCodyMKW%2FNintendo-Game-Randomizer%2Freleases&text=" + "%0a" + SplatWeaponLabel.Text + "%20" + SplatWeaponName.Text + "%0a" + SplatHeadgearLabel.Text + "%20" + SplatHeadgearName.Text + "%0a" + SplatClothingLabel.Text + "%20" + SplatClothingName.Text + "%0a" + SplatShoesLabel.Text + "%20" + SplatShoesName.Text + "%0a" + SplatStageLabel.Text + "%20" + SplatStageName.Text + "%0a" + SplatModeLabel.Text + "%20" + SplatModeName.Text + "%0aGenerated%20with&hashtags=Splatoon2%2CNintendoSwitch%2CNintendoGameRandomizer")
        End If
    End Sub

    Private Sub checkOnlyWeaponClothes_CheckedChanged(sender As Object, e As EventArgs) Handles checkOnlyWeaponClothes.CheckedChanged
        btnSplatTweet.Enabled = False
    End Sub


    Private Sub btnSmashTweet_Click(sender As Object, e As EventArgs) Handles btnSmashTweet.Click
        Process.Start("https://twitter.com/intent/tweet?url=https%3A%2F%2Fgithub.com%2FCodyMKW%2FNintendo-Game-Randomizer%2Freleases&text=" + "%0a" + SmashCharacterLabel.Text + "%20" + SmashCharacterName.Text + "%0a" + SmashStageLabel.Text + "%20" + SmashStageName.Text + "%0aGenerated%20with&hashtags=SmashBrosUltimate%2CNintendoSwitch%2CNintendoGameRandomizer")
    End Sub

    'App Settings'

    Private Sub MainForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If saveSettings.Checked = True Then
            My.Settings.MK8Character = MK8CharacterName.Text
            My.Settings.MK8Vehicle = MK8VehicleName.Text
            My.Settings.MK8Tires = MK8TiresName.Text
            My.Settings.MK8Glider = MK8GliderName.Text
            My.Settings.MK8Track = MK8TrackName.Text
            My.Settings.MK8Mode = MK8BattleModeName.Text
            My.Settings.MK8MiiName = checkCustomMiiNames.Checked
            My.Settings.MK8BattleMode = checkBattleMode.Checked
            My.Settings.MK8OnlyCharVehicle = checkOnlyCharAndParts.Checked
            My.Settings.MK8ExcludeGold = checkExcludeGold.Checked
            My.Settings.MiiName = MiiNameTextBox.Text
            My.Settings.Weapon = SplatWeaponName.Text
            My.Settings.Headgear = SplatHeadgearName.Text
            My.Settings.Shirt = SplatClothingName.Text
            My.Settings.Shoes = SplatShoesName.Text
            My.Settings.Stage = SplatStageName.Text
            My.Settings.SplatMode = SplatModeName.Text
            My.Settings.AllWeapons = checkAllWeapons.Checked
            My.Settings.OnlyShooters = checkOnlyShooters.Checked
            My.Settings.OnlyBlasters = checkOnlyBlasters.Checked
            My.Settings.OnlyRollers = checkOnlyRollers.Checked
            My.Settings.OnlySloshers = checkOnlySloshers.Checked
            My.Settings.OnlyChargers = checkOnlyChargers.Checked
            My.Settings.OnlySplatlings = checkOnlySplatlings.Checked
            My.Settings.OnlyDuelies = checkOnlyDuelies.Checked
            My.Settings.OnlyBrellas = checkOnlyBrellas.Checked
            My.Settings.OnlyWeaponClothes = checkOnlyWeaponClothes.Checked
            My.Settings.MK8Tweet = btnMK8Tweet.Enabled
            My.Settings.SplatTweet = btnSplatTweet.Enabled
            My.Settings.SaveSettings = saveSettings.Checked
            My.Settings.Theme = ThemeComboBox.Text
            My.Settings.Color = ColorComboBox.Text
            My.Settings.WarnAboutUpdate = warnAboutNewVersion.Checked
            My.Settings.SmashCharacter = SmashCharacterName.Text
            My.Settings.SmashTweet = btnSmashTweet.Enabled
            My.Settings.SmashStage = SmashStageName.Text

            My.Settings.Save()
        Else
            My.Settings.Reset()
        End If
    End Sub

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MK8CharacterName.Text = My.Settings.MK8Character
        MK8VehicleName.Text = My.Settings.MK8Vehicle
        MK8TiresName.Text = My.Settings.MK8Tires
        MK8GliderName.Text = My.Settings.MK8Glider
        MK8TrackName.Text = My.Settings.MK8Track
        MK8BattleModeName.Text = My.Settings.MK8Mode
        checkCustomMiiNames.Checked = My.Settings.MK8MiiName
        checkBattleMode.Checked = My.Settings.MK8BattleMode
        checkOnlyCharAndParts.Checked = My.Settings.MK8OnlyCharVehicle
        checkExcludeGold.Checked = My.Settings.MK8ExcludeGold
        MiiNameTextBox.Text = My.Settings.MiiName
        SplatWeaponName.Text = My.Settings.Weapon
        SplatHeadgearName.Text = My.Settings.Headgear
        SplatClothingName.Text = My.Settings.Shirt
        SplatShoesName.Text = My.Settings.Shoes
        SplatStageName.Text = My.Settings.Stage
        SplatModeName.Text = My.Settings.SplatMode
        checkAllWeapons.Checked = My.Settings.AllWeapons
        checkOnlyShooters.Checked = My.Settings.OnlyShooters
        checkOnlyBlasters.Checked = My.Settings.OnlyBlasters
        checkOnlyRollers.Checked = My.Settings.OnlyRollers
        checkOnlySloshers.Checked = My.Settings.OnlySloshers
        checkOnlyChargers.Checked = My.Settings.OnlyChargers
        checkOnlySplatlings.Checked = My.Settings.OnlySplatlings
        checkOnlyDuelies.Checked = My.Settings.OnlyDuelies
        checkOnlyBrellas.Checked = My.Settings.OnlyBrellas
        checkOnlyWeaponClothes.Checked = My.Settings.OnlyWeaponClothes
        btnMK8Tweet.Enabled = My.Settings.MK8Tweet
        btnSplatTweet.Enabled = My.Settings.SplatTweet
        saveSettings.Checked = My.Settings.SaveSettings
        ThemeComboBox.Text = My.Settings.Theme
        ColorComboBox.Text = My.Settings.Color
        warnAboutNewVersion.Checked = My.Settings.WarnAboutUpdate
        SmashCharacterName.Text = My.Settings.SmashCharacter
        btnSmashTweet.Enabled = My.Settings.SmashTweet
        SmashStageName.Text = My.Settings.SmashStage

        If checkBattleMode.Checked = True Then
            MK8TrackLabel.Text = "Course:"
            MK8BattleModeLabel.Text = "Mode:"
        End If

        If checkOnlyWeaponClothes.Checked = True Then
            SplatStageLabel.Text = " "
            SplatModeLabel.Text = " "
            SplatStageName.Text = " "
            SplatModeName.Text = " "
        End If

        If ThemeComboBox.Text = "Light" Then
            Me.MetroTabControl1.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8DeluxePage.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkExcludeGold.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnMK8Tweet.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8BattleModeName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8BattleModeLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyCharAndParts.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkBattleMode.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkCustomMiiNames.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MiiNameLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MiiNameTextBox.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MetroLabel1.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8TrackName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8GliderName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8TiresName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8VehicleName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8CharacterName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8GliderLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8TiresLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8TrackLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8VehicleLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8CharacterLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnMK8Generate.Theme = MetroFramework.MetroThemeStyle.Light
            Me.Splatoon2Page.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnSplatTweet.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlySloshers.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlySplatlings.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyBrellas.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyDuelies.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyChargers.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyRollers.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyBlasters.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyShooters.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkAllWeapons.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyWeaponClothes.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnSplatGenerate.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatModeName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatStageName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatShoesName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatClothingName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatHeadgearName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatWeaponName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatModeLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatShoesLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatClothingLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatStageLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatWeaponLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatHeadgearLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashBrosUltimatePage.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashCharacterLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashCharacterName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.AppSettings.Theme = MetroFramework.MetroThemeStyle.Light
            Me.ColorLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.ThemeLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.ColorComboBox.Theme = MetroFramework.MetroThemeStyle.Light
            Me.ThemeComboBox.Theme = MetroFramework.MetroThemeStyle.Light
            Me.saveSettings.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnExitApplication.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MetroToolTip1.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnAbout.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnDiscord.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnTwitter.Theme = MetroFramework.MetroThemeStyle.Light
            Me.warnAboutNewVersion.Theme = MetroFramework.MetroThemeStyle.Light
            Me.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnSmashGenerate.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnSmashTweet.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashStageLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashStageName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SettingsLocationLabel.Theme = MetroFramework.MetroThemeStyle.Light
        ElseIf ThemeComboBox.Text = "Dark" Then
            Me.MetroTabControl1.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8DeluxePage.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkExcludeGold.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnMK8Tweet.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8BattleModeName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8BattleModeLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyCharAndParts.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkBattleMode.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkCustomMiiNames.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MiiNameLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MiiNameTextBox.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MetroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8TrackName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8GliderName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8TiresName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8VehicleName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8CharacterName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8GliderLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8TiresLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8TrackLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8VehicleLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8CharacterLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnMK8Generate.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.Splatoon2Page.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnSplatTweet.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlySloshers.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlySplatlings.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyBrellas.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyDuelies.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyChargers.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyRollers.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyBlasters.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyShooters.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkAllWeapons.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyWeaponClothes.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnSplatGenerate.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatModeName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatStageName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatShoesName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatClothingName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatHeadgearName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatWeaponName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatModeLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatShoesLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatClothingLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatStageLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatWeaponLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatHeadgearLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashBrosUltimatePage.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashCharacterLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashCharacterName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.AppSettings.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.ColorLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.ThemeLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.ColorComboBox.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.ThemeComboBox.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.saveSettings.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnExitApplication.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MetroToolTip1.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnAbout.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnDiscord.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnTwitter.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.warnAboutNewVersion.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnSmashGenerate.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnSmashTweet.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashStageLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashStageName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SettingsLocationLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        End If

        If ColorComboBox.Text = "Black" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Black
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Black
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Black
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Black
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Black
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Black
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Black
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Black
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Black
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Black
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Black
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Black
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Black
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Black
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Black
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Black
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Black
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Black
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Black
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Black
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Black
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Black
            Me.Style = MetroFramework.MetroColorStyle.Black
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Black
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Black
        ElseIf ColorComboBox.Text = "White" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.White
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.White
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.White
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.White
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.White
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.White
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.White
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.White
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.White
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.White
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.White
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.White
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.White
            Me.SmashCharacterLabel.Style = MetroFramework.MetroColorStyle.White
            Me.SmashCharacterName.Style = MetroFramework.MetroColorStyle.White
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.White
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.White
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.White
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.White
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.White
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.White
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.White
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.White
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.White
            Me.Style = MetroFramework.MetroColorStyle.White
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.White
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.White
        ElseIf ColorComboBox.Text = "Silver" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Silver
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Silver
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Silver
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Silver
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Silver
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Silver
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Silver
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Silver
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Silver
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Silver
            Me.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Silver
        ElseIf ColorComboBox.Text = "Blue" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Blue
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Blue
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Blue
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Blue
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Blue
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Blue
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Blue
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Blue
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Blue
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Blue
            Me.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Blue
        ElseIf ColorComboBox.Text = "Green" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Green
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Green
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Green
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Green
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Green
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Green
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Green
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Green
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Green
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Green
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Green
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Green
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Green
            Me.ColorLabel.Style = MetroFramework.MetroColorStyle.Green
            Me.ThemeLabel.Style = MetroFramework.MetroColorStyle.Green
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Green
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Green
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Green
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Green
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Green
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Green
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Green
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Green
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Green
            Me.Style = MetroFramework.MetroColorStyle.Green
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Green
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Green
        ElseIf ColorComboBox.Text = "Lime" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Lime
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Lime
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Lime
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Lime
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Lime
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Lime
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Lime
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Lime
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Lime
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Lime
            Me.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Lime
        ElseIf ColorComboBox.Text = "Teal" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Teal
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Teal
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Teal
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Teal
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Teal
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Teal
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Teal
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Teal
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Teal
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Teal
            Me.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Teal
        ElseIf ColorComboBox.Text = "Orange" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Orange
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Orange
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Orange
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Orange
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Orange
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Orange
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Orange
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Orange
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Orange
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Orange
            Me.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Orange
        ElseIf ColorComboBox.Text = "Brown" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Brown
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Brown
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Brown
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Brown
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Brown
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Brown
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Brown
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Brown
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Brown
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Brown
            Me.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Brown
        ElseIf ColorComboBox.Text = "Pink" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Pink
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Pink
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Pink
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Pink
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Pink
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Pink
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Pink
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Pink
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Pink
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Pink
            Me.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Pink
        ElseIf ColorComboBox.Text = "Magenta" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Magenta
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Magenta
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Magenta
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Magenta
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Magenta
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Magenta
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Magenta
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Magenta
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Magenta
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Magenta
            Me.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Magenta
        ElseIf ColorComboBox.Text = "Purple" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Purple
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Purple
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Purple
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Purple
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Purple
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Purple
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Purple
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Purple
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Purple
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Purple
            Me.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Purple
        ElseIf ColorComboBox.Text = "Red" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Red
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Red
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Red
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Red
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Red
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Red
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Red
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Red
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Red
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Red
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Red
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Red
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Red
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Red
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Red
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Red
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Red
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Red
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Red
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Red
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Red
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Red
            Me.Style = MetroFramework.MetroColorStyle.Red
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Red
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Red
        ElseIf ColorComboBox.Text = "Yellow" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Yellow
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Yellow
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Yellow
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Yellow
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Yellow
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Yellow
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Yellow
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Yellow
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Yellow
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Yellow
            Me.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Yellow
        End If

        If warnAboutNewVersion.Checked = True Then
            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create("https://raw.githubusercontent.com/CodyMKW/Nintendo-Game-Randomizer/master/version.txt")
            Dim response As System.Net.HttpWebResponse = request.GetResponse()

            Dim sr As System.IO.StreamReader = New System.IO.StreamReader(response.GetResponseStream())

            Dim newestversion As String = sr.ReadToEnd()
            Dim currentversion As String = Application.ProductVersion

            If newestversion.Contains(currentversion) Then
                'Do Nothing'
            Else
                MsgBox("A new version of the Nintendo Game Randomizer is available to download click the About Application button then click the Check for Update button to start the download! :)", MsgBoxStyle.Information)
            End If
        End If

        MetroTabControl1.SelectedTab = MK8DeluxePage

        If saveSettings.Checked = True Then
            SettingsLocationLabel.Visible = True
            SettingsLocationLabel.Text = "You can find saved settings at: " & vbNewLine & "C:\Users\" + System.Windows.Forms.SystemInformation.UserName + "\AppData\Local\Cody's_Nintendo_Room"
        Else
            SettingsLocationLabel.Visible = False
        End If
    End Sub

    'Social media and About buttons'

    Private Sub btnAbout_Click(sender As Object, e As EventArgs) Handles btnAbout.Click
        About.Show()
    End Sub

    Private Sub btnTwitter_Click(sender As Object, e As EventArgs) Handles btnTwitter.Click
        Process.Start("https://twitter.com/CodyMKW")
    End Sub

    Private Sub btnDiscord_Click(sender As Object, e As EventArgs) Handles btnDiscord.Click
        Process.Start("https://discord.gg/Tr7McUT")
    End Sub

    Private Sub ThemeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ThemeComboBox.SelectedIndexChanged
        If ThemeComboBox.Text = "Light" Then
            Me.MetroTabControl1.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8DeluxePage.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkExcludeGold.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnMK8Tweet.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8BattleModeName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8BattleModeLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyCharAndParts.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkBattleMode.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkCustomMiiNames.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MiiNameLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MiiNameTextBox.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MetroLabel1.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8TrackName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8GliderName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8TiresName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8VehicleName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8CharacterName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8GliderLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8TiresLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8TrackLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8VehicleLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MK8CharacterLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnMK8Generate.Theme = MetroFramework.MetroThemeStyle.Light
            Me.Splatoon2Page.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnSplatTweet.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlySloshers.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlySplatlings.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyBrellas.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyDuelies.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyChargers.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyRollers.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyBlasters.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyShooters.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkAllWeapons.Theme = MetroFramework.MetroThemeStyle.Light
            Me.checkOnlyWeaponClothes.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnSplatGenerate.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatModeName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatStageName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatShoesName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatClothingName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatHeadgearName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatWeaponName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatModeLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatShoesLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatClothingLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatStageLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatWeaponLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SplatHeadgearLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashBrosUltimatePage.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashCharacterLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashCharacterName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.AppSettings.Theme = MetroFramework.MetroThemeStyle.Light
            Me.ColorLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.ThemeLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.ColorComboBox.Theme = MetroFramework.MetroThemeStyle.Light
            Me.ThemeComboBox.Theme = MetroFramework.MetroThemeStyle.Light
            Me.saveSettings.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnExitApplication.Theme = MetroFramework.MetroThemeStyle.Light
            Me.MetroToolTip1.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnAbout.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnDiscord.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnTwitter.Theme = MetroFramework.MetroThemeStyle.Light
            Me.warnAboutNewVersion.Theme = MetroFramework.MetroThemeStyle.Light
            Me.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnSmashGenerate.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnSmashTweet.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashStageLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SmashStageName.Theme = MetroFramework.MetroThemeStyle.Light
            Me.SettingsLocationLabel.Theme = MetroFramework.MetroThemeStyle.Light
            About.UpdateLabel.Theme = MetroFramework.MetroThemeStyle.Light
            About.AboutText.Theme = MetroFramework.MetroThemeStyle.Light
            About.btnAbout.Theme = MetroFramework.MetroThemeStyle.Light
            About.CheckForUpdatesBtn.Theme = MetroFramework.MetroThemeStyle.Light
            About.UpdateProgressBar.Theme = MetroFramework.MetroThemeStyle.Light
            About.ProgressLabel.Theme = MetroFramework.MetroThemeStyle.Light
            About.CurrentVersionLabel.Theme = MetroFramework.MetroThemeStyle.Light
            About.Theme = MetroFramework.MetroThemeStyle.Light
        ElseIf ThemeComboBox.Text = "Dark" Then
            Me.MetroTabControl1.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8DeluxePage.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkExcludeGold.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnMK8Tweet.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8BattleModeName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8BattleModeLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyCharAndParts.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkBattleMode.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkCustomMiiNames.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MiiNameLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MiiNameTextBox.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MetroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8TrackName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8GliderName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8TiresName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8VehicleName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8CharacterName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8GliderLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8TiresLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8TrackLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8VehicleLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MK8CharacterLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnMK8Generate.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.Splatoon2Page.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnSplatTweet.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlySloshers.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlySplatlings.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyBrellas.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyDuelies.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyChargers.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyRollers.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyBlasters.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyShooters.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkAllWeapons.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.checkOnlyWeaponClothes.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnSplatGenerate.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatModeName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatStageName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatShoesName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatClothingName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatHeadgearName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatWeaponName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatModeLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatShoesLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatClothingLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatStageLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatWeaponLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SplatHeadgearLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashBrosUltimatePage.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashCharacterLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashCharacterName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.AppSettings.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.ColorLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.ThemeLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.ColorComboBox.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.ThemeComboBox.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.saveSettings.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnExitApplication.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.MetroToolTip1.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnAbout.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnDiscord.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnTwitter.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.warnAboutNewVersion.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnSmashGenerate.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnSmashTweet.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashStageLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SmashStageName.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.SettingsLocationLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            About.UpdateLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            About.AboutText.Theme = MetroFramework.MetroThemeStyle.Dark
            About.btnAbout.Theme = MetroFramework.MetroThemeStyle.Dark
            About.CheckForUpdatesBtn.Theme = MetroFramework.MetroThemeStyle.Dark
            About.UpdateProgressBar.Theme = MetroFramework.MetroThemeStyle.Dark
            About.ProgressLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            About.CurrentVersionLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            About.Theme = MetroFramework.MetroThemeStyle.Dark
        End If
    End Sub

    Private Sub ColorComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ColorComboBox.SelectedIndexChanged
        If ColorComboBox.Text = "Black" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Black
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Black
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Black
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Black
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Black
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Black
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Black
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Black
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Black
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Black
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Black
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Black
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Black
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Black
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Black
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Black
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Black
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Black
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Black
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Black
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Black
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Black
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Black
            Me.Style = MetroFramework.MetroColorStyle.Black
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Black
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Black
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Black
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Black
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Black
            About.Style = MetroFramework.MetroColorStyle.Black
        ElseIf ColorComboBox.Text = "White" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.White
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.White
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.White
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.White
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.White
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.White
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.White
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.White
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.White
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.White
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.White
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.White
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.White
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.White
            Me.SmashCharacterLabel.Style = MetroFramework.MetroColorStyle.White
            Me.SmashCharacterName.Style = MetroFramework.MetroColorStyle.White
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.White
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.White
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.White
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.White
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.White
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.White
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.White
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.White
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.White
            Me.Style = MetroFramework.MetroColorStyle.White
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.White
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.White
            About.btnAbout.Style = MetroFramework.MetroColorStyle.White
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.White
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.White
            About.Style = MetroFramework.MetroColorStyle.White
        ElseIf ColorComboBox.Text = "Silver" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Silver
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Silver
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Silver
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Silver
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Silver
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Silver
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Silver
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Silver
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Silver
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Silver
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Silver
            Me.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Silver
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Silver
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Silver
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Silver
            About.Style = MetroFramework.MetroColorStyle.Silver
        ElseIf ColorComboBox.Text = "Blue" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Blue
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Blue
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Blue
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Blue
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Blue
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Blue
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Blue
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Blue
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Blue
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Blue
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Blue
            Me.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Blue
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Blue
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Blue
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Blue
            About.Style = MetroFramework.MetroColorStyle.Blue
        ElseIf ColorComboBox.Text = "Green" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Green
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Green
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Green
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Green
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Green
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Green
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Green
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Green
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Green
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Green
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Green
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Green
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Green
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Green
            Me.ColorLabel.Style = MetroFramework.MetroColorStyle.Green
            Me.ThemeLabel.Style = MetroFramework.MetroColorStyle.Green
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Green
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Green
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Green
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Green
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Green
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Green
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Green
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Green
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Green
            Me.Style = MetroFramework.MetroColorStyle.Green
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Green
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Green
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Green
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Green
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Green
            About.Style = MetroFramework.MetroColorStyle.Green
        ElseIf ColorComboBox.Text = "Lime" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Lime
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Lime
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Lime
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Lime
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Lime
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Lime
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Lime
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Lime
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Lime
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Lime
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Lime
            Me.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Lime
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Lime
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Lime
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Lime
            About.Style = MetroFramework.MetroColorStyle.Lime
        ElseIf ColorComboBox.Text = "Teal" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Teal
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Teal
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Teal
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Teal
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Teal
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Teal
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Teal
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Teal
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Teal
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Teal
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Teal
            Me.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Teal
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Teal
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Teal
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Teal
            About.Style = MetroFramework.MetroColorStyle.Teal
        ElseIf ColorComboBox.Text = "Orange" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Orange
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Orange
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Orange
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Orange
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Orange
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Orange
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Orange
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Orange
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Orange
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Orange
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Orange
            Me.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Orange
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Orange
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Orange
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Orange
            About.Style = MetroFramework.MetroColorStyle.Orange
        ElseIf ColorComboBox.Text = "Brown" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Brown
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Brown
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Brown
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Brown
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Brown
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Brown
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Brown
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Brown
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Brown
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Brown
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Brown
            Me.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Brown
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Brown
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Brown
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Brown
            About.Style = MetroFramework.MetroColorStyle.Brown
        ElseIf ColorComboBox.Text = "Pink" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Pink
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Pink
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Pink
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Pink
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Pink
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Pink
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Pink
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Pink
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Pink
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Pink
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Pink
            Me.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Pink
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Pink
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Pink
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Pink
            About.Style = MetroFramework.MetroColorStyle.Pink
        ElseIf ColorComboBox.Text = "Magenta" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Magenta
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Magenta
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Magenta
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Magenta
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Magenta
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Magenta
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Magenta
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Magenta
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Magenta
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Magenta
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Magenta
            Me.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Magenta
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Magenta
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Magenta
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Magenta
            About.Style = MetroFramework.MetroColorStyle.Magenta
        ElseIf ColorComboBox.Text = "Purple" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Purple
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Purple
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Purple
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Purple
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Purple
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Purple
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Purple
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Purple
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Purple
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Purple
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Purple
            Me.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Purple
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Purple
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Purple
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Purple
            About.Style = MetroFramework.MetroColorStyle.Purple
        ElseIf ColorComboBox.Text = "Red" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Red
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Red
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Red
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Red
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Red
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Red
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Red
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Red
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Red
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Red
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Red
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Red
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Red
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Red
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Red
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Red
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Red
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Red
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Red
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Red
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Red
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Red
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Red
            Me.Style = MetroFramework.MetroColorStyle.Red
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Red
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Red
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Red
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Red
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Red
            About.Style = MetroFramework.MetroColorStyle.Red
        ElseIf ColorComboBox.Text = "Yellow" Then
            Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Yellow
            Me.MK8DeluxePage.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkExcludeGold.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnMK8Tweet.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyCharAndParts.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkBattleMode.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkCustomMiiNames.Style = MetroFramework.MetroColorStyle.Yellow
            Me.MiiNameTextBox.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnMK8Generate.Style = MetroFramework.MetroColorStyle.Yellow
            Me.Splatoon2Page.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnSplatTweet.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlySloshers.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlySplatlings.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyBrellas.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyDuelies.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyChargers.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyRollers.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyBlasters.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyShooters.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkAllWeapons.Style = MetroFramework.MetroColorStyle.Yellow
            Me.checkOnlyWeaponClothes.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnSplatGenerate.Style = MetroFramework.MetroColorStyle.Yellow
            Me.SmashBrosUltimatePage.Style = MetroFramework.MetroColorStyle.Yellow
            Me.ColorComboBox.Style = MetroFramework.MetroColorStyle.Yellow
            Me.ThemeComboBox.Style = MetroFramework.MetroColorStyle.Yellow
            Me.saveSettings.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnExitApplication.Style = MetroFramework.MetroColorStyle.Yellow
            Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnDiscord.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnTwitter.Style = MetroFramework.MetroColorStyle.Yellow
            Me.warnAboutNewVersion.Style = MetroFramework.MetroColorStyle.Yellow
            Me.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnSmashGenerate.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnSmashTweet.Style = MetroFramework.MetroColorStyle.Yellow
            About.btnAbout.Style = MetroFramework.MetroColorStyle.Yellow
            About.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Yellow
            About.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Yellow
            About.Style = MetroFramework.MetroColorStyle.Yellow
        End If
    End Sub

    'Smash Bros tab'

    Private Sub btnSmashGenerate_Click(sender As Object, e As EventArgs) Handles btnSmashGenerate.Click
        Dim SmashCharacterNumber As Integer = 69
        Dim SmashStageNumber As Integer = 104
        Dim r As New Random
        Dim RandomSmashCharacter
        Dim RandomSmashStage
        RandomSmashCharacter = r.Next(SmashCharacterNumber)
        RandomSmashStage = r.Next(SmashStageNumber)

        Select Case RandomSmashCharacter
            Case 1
                SmashCharacterName.Text = "Mario"
            Case 2
                SmashCharacterName.Text = "Donkey Kong"
            Case 3
                SmashCharacterName.Text = "Link"
            Case 4
                SmashCharacterName.Text = "Samus"
            Case 5
                SmashCharacterName.Text = "Dark Samus"
            Case 6
                SmashCharacterName.Text = "Yoshi"
            Case 7
                SmashCharacterName.Text = "Kirby"
            Case 8
                SmashCharacterName.Text = "Fox"
            Case 9
                SmashCharacterName.Text = "Pikachu"
            Case 10
                SmashCharacterName.Text = "Luigi"
            Case 11
                SmashCharacterName.Text = "Ness"
            Case 12
                SmashCharacterName.Text = "Captain Falcon"
            Case 13
                SmashCharacterName.Text = "Jigglypuff"
            Case 14
                SmashCharacterName.Text = "Peach"
            Case 15
                SmashCharacterName.Text = "Daisy"
            Case 16
                SmashCharacterName.Text = "Bowser"
            Case 17
                SmashCharacterName.Text = "Ice Climbers"
            Case 18
                SmashCharacterName.Text = "Sheik"
            Case 19
                SmashCharacterName.Text = "Zelda"
            Case 20
                SmashCharacterName.Text = "Dr. Mario"
            Case 21
                SmashCharacterName.Text = "Pichu"
            Case 22
                SmashCharacterName.Text = "Falco"
            Case 23
                SmashCharacterName.Text = "Marth"
            Case 24
                SmashCharacterName.Text = "Lucina"
            Case 25
                SmashCharacterName.Text = "Young Link"
            Case 26
                SmashCharacterName.Text = "Ganondorf"
            Case 27
                SmashCharacterName.Text = "Mewtwo"
            Case 28
                SmashCharacterName.Text = "Roy"
            Case 29
                SmashCharacterName.Text = "Chrom"
            Case 30
                SmashCharacterName.Text = "Mr. Game and Watch"
            Case 31
                SmashCharacterName.Text = "Meta Knight"
            Case 32
                SmashCharacterName.Text = "Pit"
            Case 33
                SmashCharacterName.Text = "Dark Pit"
            Case 34
                SmashCharacterName.Text = "Zero Suit Samus"
            Case 35
                SmashCharacterName.Text = "Wario"
            Case 36
                SmashCharacterName.Text = "Snake"
            Case 37
                SmashCharacterName.Text = "Ike"
            Case 38
                SmashCharacterName.Text = "Pokemon Trainer"
            Case 39
                SmashCharacterName.Text = "Lucas"
            Case 40
                SmashCharacterName.Text = "Sonic"
            Case 41
                SmashCharacterName.Text = "King Dedede"
            Case 42
                SmashCharacterName.Text = "Lucario"
            Case 43
                SmashCharacterName.Text = "R.O.B."
            Case 44
                SmashCharacterName.Text = "Toon Link"
            Case 45
                SmashCharacterName.Text = "Wolf"
            Case 46
                SmashCharacterName.Text = "Villager"
            Case 47
                SmashCharacterName.Text = "Megaman"
            Case 48
                SmashCharacterName.Text = "Wii Fit Trainer"
            Case 49
                SmashCharacterName.Text = "Rosalina and Luma"
            Case 50
                SmashCharacterName.Text = "Little Mac"
            Case 51
                SmashCharacterName.Text = "Greninja"
            Case 52
                SmashCharacterName.Text = "Mii Fighter"
            Case 53
                SmashCharacterName.Text = "Palutena"
            Case 54
                SmashCharacterName.Text = "Pac-Man"
            Case 55
                SmashCharacterName.Text = "Robin"
            Case 56
                SmashCharacterName.Text = "Shulk"
            Case 57
                SmashCharacterName.Text = "Bowser Jr."
            Case 58
                SmashCharacterName.Text = "Duck Hunt"
            Case 59
                SmashCharacterName.Text = "Ryu"
            Case 60
                SmashCharacterName.Text = "Cloud"
            Case 61
                SmashCharacterName.Text = "Corrin"
            Case 62
                SmashCharacterName.Text = "Bayonetta"
            Case 63
                SmashCharacterName.Text = "Inkling"
            Case 64
                SmashCharacterName.Text = "Ridley"
            Case 65
                SmashCharacterName.Text = "Simon"
            Case 66
                SmashCharacterName.Text = "Richter"
            Case 67
                SmashCharacterName.Text = "King K. Rool"
            Case 68
                SmashCharacterName.Text = "Isabelle"
        End Select

        Select Case RandomSmashStage
            Case 1
                SmashStageName.Text = "Battlefield"
            Case 2
                SmashStageName.Text = "Big Battlefield"
            Case 3
                SmashStageName.Text = "Final Destination"
            Case 4
                SmashStageName.Text = "New Donk City Hall"
            Case 5
                SmashStageName.Text = "Great Plateau Tower"
            Case 6
                SmashStageName.Text = "Moray Towers"
            Case 7
                SmashStageName.Text = "Dracula's Castle"
            Case 8
                SmashStageName.Text = "(N64) Peach's Castle"
            Case 9
                SmashStageName.Text = "(N64) Mushroom Kingdom"
            Case 10
                SmashStageName.Text = "(N64) Super Happy Tree"
            Case 11
                SmashStageName.Text = "(N64) Kongo Jungle"
            Case 12
                SmashStageName.Text = "(N64) Hyrule Castle"
            Case 13
                SmashStageName.Text = "(N64) Dream Land"
            Case 14
                SmashStageName.Text = "(N64) Saffron City"
            Case 15
                SmashStageName.Text = "(Melee) Princess Peach's Castle"
            Case 16
                SmashStageName.Text = "(Melee) Rainbow Cruise"
            Case 17
                SmashStageName.Text = "(Melee) Yoshi's Island"
            Case 18
                SmashStageName.Text = "(Melee) Yoshi's Story"
            Case 19
                SmashStageName.Text = "(Melee) Kongo Falls"
            Case 20
                SmashStageName.Text = "(Melee) Jungle Japes"
            Case 21
                SmashStageName.Text = "(Melee) Great Bay"
            Case 22
                SmashStageName.Text = "(Melee) Temple"
            Case 23
                SmashStageName.Text = "(Melee) Brinstar"
            Case 24
                SmashStageName.Text = "(Melee) Brinstar Depths"
            Case 25
                SmashStageName.Text = "(Melee) Fountain of Dreams"
            Case 26
                SmashStageName.Text = "(Melee) Green Greens"
            Case 27
                SmashStageName.Text = "(Melee) Corneria"
            Case 28
                SmashStageName.Text = "(Melee) Venom"
            Case 29
                SmashStageName.Text = "(Melee) Pokemon Stadium"
            Case 30
                SmashStageName.Text = "(Melee) Onett"
            Case 31
                SmashStageName.Text = "(Melee) Fourside"
            Case 32
                SmashStageName.Text = "(Melee) Big Blue"
            Case 33
                SmashStageName.Text = "(Brawl) Delfino Plaza"
            Case 34
                SmashStageName.Text = "(Brawl) Luigi's Mansion"
            Case 35
                SmashStageName.Text = "(Brawl) Mushroomy Kingdom"
            Case 36
                SmashStageName.Text = "(Brawl) Figure-8 Circuit"
            Case 37
                SmashStageName.Text = "(Brawl) Mario Bros."
            Case 38
                SmashStageName.Text = "(Brawl) Yoshi’s Island"
            Case 39
                SmashStageName.Text = "(Brawl) 75m"
            Case 40
                SmashStageName.Text = "(Brawl) Bridge of Eldin"
            Case 41
                SmashStageName.Text = "(Brawl) Pirate Ship"
            Case 42
                SmashStageName.Text = "(Brawl) Norfair"
            Case 43
                SmashStageName.Text = "(Brawl) Frigate Orpheon"
            Case 44
                SmashStageName.Text = "(Brawl) Halberd"
            Case 45
                SmashStageName.Text = "(Brawl) Lylat Cruise"
            Case 46
                SmashStageName.Text = "(Brawl) Pokemon Stadium 2"
            Case 47
                SmashStageName.Text = "(Brawl) Spear Pillar"
            Case 48
                SmashStageName.Text = "(Brawl) Port Town Aero Dive"
            Case 49
                SmashStageName.Text = "(Brawl) New Pork City"
            Case 50
                SmashStageName.Text = "(Brawl) Summit"
            Case 51
                SmashStageName.Text = "(Brawl) Castle Siege"
            Case 52
                SmashStageName.Text = "(Brawl) Skyworld"
            Case 53
                SmashStageName.Text = "(Brawl) WarioWare, Inc."
            Case 54
                SmashStageName.Text = "(Brawl) Distant Planet"
            Case 55
                SmashStageName.Text = "(Brawl) Shadow Moses Island"
            Case 56
                SmashStageName.Text = "(Brawl) Green Hill Zone"
            Case 57
                SmashStageName.Text = "(Brawl) Smashville"
            Case 58
                SmashStageName.Text = "(Brawl) Hanenbow"
            Case 59
                SmashStageName.Text = "(3DS) 3D Land"
            Case 60
                SmashStageName.Text = "(3DS) Golden Plains"
            Case 61
                SmashStageName.Text = "(3DS) Paper Mario"
            Case 62
                SmashStageName.Text = "(3DS) Gerudo Valley"
            Case 63
                SmashStageName.Text = "(3DS) Spirit Train"
            Case 64
                SmashStageName.Text = "(3DS) Dream Land GB"
            Case 65
                SmashStageName.Text = "(3DS) Unova Pokemon League"
            Case 66
                SmashStageName.Text = "(3DS) Prism Tower"
            Case 67
                SmashStageName.Text = "(3DS) Mute City SNES"
            Case 68
                SmashStageName.Text = "(3DS) Magicant"
            Case 69
                SmashStageName.Text = "(3DS) Arena Ferox"
            Case 70
                SmashStageName.Text = "(3DS) Reset Bomb Forest"
            Case 71
                SmashStageName.Text = "(3DS) Tortimer Island"
            Case 72
                SmashStageName.Text = "(3DS) Balloon Fight"
            Case 73
                SmashStageName.Text = "(3DS) Living Room"
            Case 74
                SmashStageName.Text = "(3DS) Find Mii"
            Case 75
                SmashStageName.Text = "(3DS) Tomodachi Life"
            Case 76
                SmashStageName.Text = "(3DS) PictoChat 2"
            Case 78
                SmashStageName.Text = "(Wii U) Mushroom Kingdom U"
            Case 79
                SmashStageName.Text = "(Wii U) Mario Galaxy"
            Case 80
                SmashStageName.Text = "(Wii U) Mario Circuit"
            Case 81
                SmashStageName.Text = "(Wii U) Skyloft"
            Case 82
                SmashStageName.Text = "(Wii U) The Great Cave Offensive"
            Case 83
                SmashStageName.Text = "(Wii U) Kalos Pokemon League"
            Case 84
                SmashStageName.Text = "(Wii U) Coliseum"
            Case 85
                SmashStageName.Text = "(Wii U) Flat Zone X"
            Case 86
                SmashStageName.Text = "(Wii U) Palutena's Temple"
            Case 87
                SmashStageName.Text = "(Wii U) Gamer"
            Case 88
                SmashStageName.Text = "(Wii U) Garden of Hope"
            Case 89
                SmashStageName.Text = "(Wii U) Town and City"
            Case 90
                SmashStageName.Text = "(Wii U) Wii Fit Studio"
            Case 91
                SmashStageName.Text = "(Wii U) Wrecking Crew"
            Case 92
                SmashStageName.Text = "(Wii U) Windy Hill Zone"
            Case 93
                SmashStageName.Text = "(Wii U) Pac-Land"
            Case 94
                SmashStageName.Text = "(Wii U) Pilotwings"
            Case 95
                SmashStageName.Text = "(Wii U) Wuhu Island"
            Case 96
                SmashStageName.Text = "(3DS/Wii U) Super Mario Maker"
            Case 97
                SmashStageName.Text = "(3DS/Wii U) Boxing Ring"
            Case 98
                SmashStageName.Text = "(3DS/Wii U) Gaur Plain"
            Case 99
                SmashStageName.Text = "(3DS/Wii U) Duck Hunt"
            Case 100
                SmashStageName.Text = "(3DS/Wii U) Wily Castle"
            Case 101
                SmashStageName.Text = "(3DS/Wii U) Suzaku Castle"
            Case 102
                SmashStageName.Text = "(3DS/Wii U) Midgar"
            Case 103
                SmashStageName.Text = "(3DS/Wii U) Umbra Clock Tower"
        End Select

        btnSmashTweet.Enabled = True
    End Sub

    Private Sub saveSettings_CheckedChanged(sender As Object, e As EventArgs) Handles saveSettings.CheckedChanged
        If saveSettings.Checked = True Then
            SettingsLocationLabel.Visible = True
            SettingsLocationLabel.Text = "You can find saved settings at: " & vbNewLine & "C:\Users\" + System.Windows.Forms.SystemInformation.UserName + "\AppData\Local\Cody's_Nintendo_Room"
        Else
            SettingsLocationLabel.Visible = False
        End If
    End Sub
End Class
