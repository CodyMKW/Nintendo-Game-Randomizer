﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class About
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(About))
        Me.UpdateLabel = New MetroFramework.Controls.MetroLabel()
        Me.AboutText = New MetroFramework.Controls.MetroTextBox()
        Me.btnAbout = New MetroFramework.Controls.MetroButton()
        Me.UpdateProgressBar = New MetroFramework.Controls.MetroProgressBar()
        Me.UpdateBrowser = New System.Windows.Forms.WebBrowser()
        Me.UpdateTimer = New System.Windows.Forms.Timer(Me.components)
        Me.CheckForUpdatesBtn = New MetroFramework.Controls.MetroButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ProgressLabel = New MetroFramework.Controls.MetroLabel()
        Me.CurrentVersionLabel = New MetroFramework.Controls.MetroLabel()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UpdateLabel
        '
        Me.UpdateLabel.Location = New System.Drawing.Point(23, 331)
        Me.UpdateLabel.Name = "UpdateLabel"
        Me.UpdateLabel.Size = New System.Drawing.Size(331, 19)
        Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.UpdateLabel.TabIndex = 2
        Me.UpdateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.UpdateLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'AboutText
        '
        Me.AboutText.Location = New System.Drawing.Point(23, 247)
        Me.AboutText.Multiline = True
        Me.AboutText.Name = "AboutText"
        Me.AboutText.ReadOnly = True
        Me.AboutText.Size = New System.Drawing.Size(353, 52)
        Me.AboutText.Style = MetroFramework.MetroColorStyle.Green
        Me.AboutText.TabIndex = 3
        Me.AboutText.TabStop = False
        Me.AboutText.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'btnAbout
        '
        Me.btnAbout.Location = New System.Drawing.Point(217, 353)
        Me.btnAbout.Name = "btnAbout"
        Me.btnAbout.Size = New System.Drawing.Size(159, 23)
        Me.btnAbout.Style = MetroFramework.MetroColorStyle.Green
        Me.btnAbout.TabIndex = 8
        Me.btnAbout.Text = "Close"
        Me.btnAbout.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'UpdateProgressBar
        '
        Me.UpdateProgressBar.Location = New System.Drawing.Point(217, 305)
        Me.UpdateProgressBar.Name = "UpdateProgressBar"
        Me.UpdateProgressBar.Size = New System.Drawing.Size(159, 23)
        Me.UpdateProgressBar.TabIndex = 10
        Me.UpdateProgressBar.Visible = False
        '
        'UpdateBrowser
        '
        Me.UpdateBrowser.Location = New System.Drawing.Point(527, 203)
        Me.UpdateBrowser.MinimumSize = New System.Drawing.Size(20, 20)
        Me.UpdateBrowser.Name = "UpdateBrowser"
        Me.UpdateBrowser.Size = New System.Drawing.Size(63, 38)
        Me.UpdateBrowser.TabIndex = 11
        '
        'UpdateTimer
        '
        '
        'CheckForUpdatesBtn
        '
        Me.CheckForUpdatesBtn.Location = New System.Drawing.Point(23, 353)
        Me.CheckForUpdatesBtn.Name = "CheckForUpdatesBtn"
        Me.CheckForUpdatesBtn.Size = New System.Drawing.Size(188, 23)
        Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Green
        Me.CheckForUpdatesBtn.TabIndex = 12
        Me.CheckForUpdatesBtn.Text = "Check for updates"
        Me.CheckForUpdatesBtn.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(23, 63)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(353, 178)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'ProgressLabel
        '
        Me.ProgressLabel.BackColor = System.Drawing.Color.Transparent
        Me.ProgressLabel.ForeColor = System.Drawing.Color.Transparent
        Me.ProgressLabel.Location = New System.Drawing.Point(284, 331)
        Me.ProgressLabel.Name = "ProgressLabel"
        Me.ProgressLabel.Size = New System.Drawing.Size(49, 19)
        Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.ProgressLabel.TabIndex = 13
        Me.ProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ProgressLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.ProgressLabel.Visible = False
        '
        'CurrentVersionLabel
        '
        Me.CurrentVersionLabel.Location = New System.Drawing.Point(23, 305)
        Me.CurrentVersionLabel.Name = "CurrentVersionLabel"
        Me.CurrentVersionLabel.Size = New System.Drawing.Size(188, 19)
        Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Green
        Me.CurrentVersionLabel.TabIndex = 14
        Me.CurrentVersionLabel.Text = "CurrentLabel"
        Me.CurrentVersionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CurrentVersionLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'About
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(399, 383)
        Me.ControlBox = False
        Me.Controls.Add(Me.CurrentVersionLabel)
        Me.Controls.Add(Me.ProgressLabel)
        Me.Controls.Add(Me.CheckForUpdatesBtn)
        Me.Controls.Add(Me.UpdateBrowser)
        Me.Controls.Add(Me.UpdateProgressBar)
        Me.Controls.Add(Me.btnAbout)
        Me.Controls.Add(Me.AboutText)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.UpdateLabel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "About"
        Me.Resizable = False
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Style = MetroFramework.MetroColorStyle.Green
        Me.Text = "About"
        Me.TextAlign = System.Windows.Forms.VisualStyles.HorizontalAlign.Center
        Me.Theme = MetroFramework.MetroThemeStyle.Dark
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents UpdateLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents AboutText As MetroFramework.Controls.MetroTextBox
    Friend WithEvents btnAbout As MetroFramework.Controls.MetroButton
    Friend WithEvents UpdateProgressBar As MetroFramework.Controls.MetroProgressBar
    Friend WithEvents UpdateBrowser As WebBrowser
    Friend WithEvents UpdateTimer As Timer
    Friend WithEvents CheckForUpdatesBtn As MetroFramework.Controls.MetroButton
    Friend WithEvents ProgressLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CurrentVersionLabel As MetroFramework.Controls.MetroLabel
End Class
