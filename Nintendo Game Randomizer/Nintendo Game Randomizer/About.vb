﻿Public Class About
    Private Sub About_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        AboutText.Text = My.Application.Info.Description.ToString()
        CurrentVersionLabel.Text = "Your current version is: " + My.Application.Info.Version.ToString

        If MainForm.ThemeComboBox.Text = "Light" Then
            Me.UpdateLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.AboutText.Theme = MetroFramework.MetroThemeStyle.Light
            Me.btnAbout.Theme = MetroFramework.MetroThemeStyle.Light
            Me.Theme = MetroFramework.MetroThemeStyle.Light
            Me.CheckForUpdatesBtn.Theme = MetroFramework.MetroThemeStyle.Light
            Me.UpdateProgressBar.Theme = MetroFramework.MetroThemeStyle.Light
            Me.ProgressLabel.Theme = MetroFramework.MetroThemeStyle.Light
            Me.CurrentVersionLabel.Theme = MetroFramework.MetroThemeStyle.Light
        ElseIf MainForm.ThemeComboBox.Text = "Dark" Then
            Me.UpdateLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.AboutText.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.btnAbout.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.CheckForUpdatesBtn.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.UpdateProgressBar.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.ProgressLabel.Theme = MetroFramework.MetroThemeStyle.Dark
            Me.CurrentVersionLabel.Theme = MetroFramework.MetroThemeStyle.Dark
        End If

        If MainForm.ColorComboBox.Text = "Black" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Black
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Black
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Black
            Me.Style = MetroFramework.MetroColorStyle.Black
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Black
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Black
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Black
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Black
        ElseIf MainForm.ColorComboBox.Text = "White" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.White
            Me.AboutText.Style = MetroFramework.MetroColorStyle.White
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.White
            Me.Style = MetroFramework.MetroColorStyle.White
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.White
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.White
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.White
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.White
        ElseIf MainForm.ColorComboBox.Text = "Silver" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Silver
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Silver
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Silver
            Me.Style = MetroFramework.MetroColorStyle.Silver
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Silver
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Silver
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Silver
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Silver
        ElseIf MainForm.ColorComboBox.Text = "Blue" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Blue
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Blue
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Blue
            Me.Style = MetroFramework.MetroColorStyle.Blue
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Blue
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Blue
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Blue
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Blue
        ElseIf MainForm.ColorComboBox.Text = "Green" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Green
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Green
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Green
            Me.Style = MetroFramework.MetroColorStyle.Green
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Green
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Green
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Green
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Green
        ElseIf MainForm.ColorComboBox.Text = "Lime" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Lime
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Lime
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Lime
            Me.Style = MetroFramework.MetroColorStyle.Lime
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Lime
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Lime
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Lime
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Lime
        ElseIf MainForm.ColorComboBox.Text = "Teal" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Teal
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Teal
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Teal
            Me.Style = MetroFramework.MetroColorStyle.Teal
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Teal
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Teal
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Teal
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Teal
        ElseIf MainForm.ColorComboBox.Text = "Orange" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Orange
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Orange
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Orange
            Me.Style = MetroFramework.MetroColorStyle.Orange
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Orange
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Orange
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Orange
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Orange
        ElseIf MainForm.ColorComboBox.Text = "Brown" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Brown
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Brown
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Brown
            Me.Style = MetroFramework.MetroColorStyle.Brown
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Brown
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Brown
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Brown
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Brown
        ElseIf MainForm.ColorComboBox.Text = "Pink" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Pink
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Pink
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Pink
            Me.Style = MetroFramework.MetroColorStyle.Pink
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Pink
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Pink
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Pink
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Pink
        ElseIf MainForm.ColorComboBox.Text = "Magenta" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Magenta
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Magenta
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Magenta
            Me.Style = MetroFramework.MetroColorStyle.Magenta
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Magenta
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Magenta
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Magenta
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Magenta
        ElseIf MainForm.ColorComboBox.Text = "Purple" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Purple
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Purple
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Purple
            Me.Style = MetroFramework.MetroColorStyle.Purple
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Purple
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Purple
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Purple
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Purple
        ElseIf MainForm.ColorComboBox.Text = "Red" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Red
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Red
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Red
            Me.Style = MetroFramework.MetroColorStyle.Red
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Red
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Red
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Red
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Red
        ElseIf MainForm.ColorComboBox.Text = "Yellow" Then
            Me.UpdateLabel.Style = MetroFramework.MetroColorStyle.Yellow
            Me.AboutText.Style = MetroFramework.MetroColorStyle.Yellow
            Me.btnAbout.Style = MetroFramework.MetroColorStyle.Yellow
            Me.CheckForUpdatesBtn.Style = MetroFramework.MetroColorStyle.Yellow
            Me.UpdateProgressBar.Style = MetroFramework.MetroColorStyle.Yellow
            Me.ProgressLabel.Style = MetroFramework.MetroColorStyle.Yellow
            Me.CurrentVersionLabel.Style = MetroFramework.MetroColorStyle.Yellow
            Me.Style = MetroFramework.MetroColorStyle.Yellow
        End If
    End Sub

    Private Sub btnAbout_Click(sender As Object, e As EventArgs) Handles btnAbout.Click
        MainForm.Focus()
        Me.Close()
    End Sub

    Private Sub UpdateTimer_Tick(sender As Object, e As EventArgs) Handles UpdateTimer.Tick
        UpdateProgressBar.Increment(5)
        ProgressLabel.Text = UpdateProgressBar.Value & "%"
        If UpdateProgressBar.Value = 100 Then
            UpdateTimer.Stop()
            Try
                If UpdateProgressBar.Value = 100 Then
                    Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create("https://raw.githubusercontent.com/CodyMKW/Nintendo-Game-Randomizer/master/version.txt")
                    Dim response As System.Net.HttpWebResponse = request.GetResponse()

                    Dim sr As System.IO.StreamReader = New System.IO.StreamReader(response.GetResponseStream())

                    Dim newestversion As String = sr.ReadToEnd()
                    Dim currentversion As String = Application.ProductVersion
                    If newestversion.Contains(currentversion) Then
                        UpdateLabel.Text = "You already have the latest version!"
                    Else
                        UpdateLabel.Text = ("Downloading update...")
                        UpdateBrowser.Navigate("https://github.com/CodyMKW/Nintendo-Game-Randomizer/blob/master/Nintendo%20Game%20Randomizer%20Setup.exe?raw=true")
                    End If
                End If
            Catch ex As Exception
                UpdateProgressBar.Hide()
                ProgressLabel.Hide()
                UpdateLabel.Text = ("No internet connection can't check for updates! :'(")
            End Try
        End If
    End Sub

    Private Sub CheckForUpdatesBtn_Click(sender As Object, e As EventArgs) Handles CheckForUpdatesBtn.Click, CheckForUpdatesBtn.Click
        CheckForUpdatesBtn.Enabled = False
        UpdateProgressBar.Visible = True
        ProgressLabel.Visible = True
        UpdateLabel.Text = "Checking for updates..."
        UpdateTimer.Start()
        ProgressLabel.Text = UpdateProgressBar.Value & "%"
        CheckForUpdates()
    End Sub

    Public Sub CheckForUpdates()
        If UpdateProgressBar.Value = 100 Then
            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create("https://raw.githubusercontent.com/CodyMKW/Nintendo-Game-Randomizer/master/version.txt")
            Dim response As System.Net.HttpWebResponse = request.GetResponse()

            Dim sr As System.IO.StreamReader = New System.IO.StreamReader(response.GetResponseStream())

            Dim newestversion As String = sr.ReadToEnd()
            Dim currentversion As String = Application.ProductVersion
            If newestversion.Contains(currentversion) Then
                UpdateLabel.Text = "Your current version is: " + My.Application.Info.Version.ToString
            Else
                UpdateLabel.Text = ("Downloading update...")
                UpdateBrowser.Navigate("https://github.com/CodyMKW/Nintendo-Game-Randomizer/blob/master/Nintendo%20Game%20Randomizer%20Setup.exe?raw=true")
            End If
        End If
    End Sub
End Class